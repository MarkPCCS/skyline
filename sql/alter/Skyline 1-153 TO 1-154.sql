# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.153');


# ---------------------------------------------------------------------- #
# Create table "help_text"                                                     #
# ---------------------------------------------------------------------- #
CREATE TABLE `help_text` (
	`HelpTextID` INT(11) NOT NULL AUTO_INCREMENT,
	`HelpTextCode` VARCHAR(64) NOT NULL,
	`HelpTextTitle` VARCHAR(255) NULL DEFAULT NULL,
	`HelpText` TEXT NULL,
	`CreatedDate` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00',
	`EndDate` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00',
	`Status` ENUM('Active','In-active') NULL DEFAULT 'Active',
	`ModifiedUserID` INT(11) NULL DEFAULT NULL,
	`ModifiedDate` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`HelpTextID`),
	UNIQUE INDEX `HelpTextCode` (`HelpTextCode`)
)
COMMENT='This table contains help text for each element.'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.154');




