<?php

/**
 * CustomSoapController.class.php
 * 
 * Generic SOAP Controller to allow specfic implementations.
 * 
 * This class should be derived to provide a class with the code for the calls
 * The derived class should only contain a decleration for each process required
 * and a decleration of the protected variable $wsdl to point at the location of 
 * the wsdl file (must be in {application}/wsdl)
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link 
 * @version    1.01
 * 
 * Changes
 * Date        Version Author                Reason
 * 22/01/2013  1.00    Andrew J. Williams    Initial Version
 * 21/02/2013  1.01    Andrew J. Williams    Custom WSDL generation
 ******************************************************************************/

require_once('CustomController.class.php');

abstract class CustomSOAPController extends CustomController {
    public $config;  
    public $session;
    public $messages;
    protected $server;                                                          /* Soap Server object - only accessable from derived classes */
    protected $wsdl;                                                            /* Must be defined in dervived class to give wsdl location */
    protected $serverpath;
     
    public function __construct() {
        parent::__construct(); 

        $this->config = $this->readConfig('application.ini');                   /* Read Application Config file. */
        $this->session = $this->loadModel('Session');                           /* Initialise Session Model */
        $this->messages = $this->loadModel('Messages');                         /* Initialise Messages Model */

        
        if ($this->debug == false) {                                            /* Check debug status */
            ini_set("soap.wsdl_cache_enabled", "1");                            /* Debug mode - enable WSDL cache */            
        } else {
            ini_set("soap.wsdl_cache_enabled", "0");                            /* Debug mode - disabling WSDL cache */
        }
    }
    
    public function indexAction() {       
        $this->server = new SoapServer(APPLICATION_PATH.'/wsdl/'.$this->wsdl);

        $this->server->setClass(get_class($this));
        $this->server->handle(); 
    }
    
    abstract function authorise($pass);
    
        
    public function WSDLAction() {
        
    /*****************************************************************************
     * To access this WSDL specification run via: /wsdl.php?WSDL
     * Any other access to this WSDL will display as a HTML document
     * 
     * 2013 (C) Copyright Lyndon Leverington / DarkerWhite
     *****************************************************************************
     * Set up the web service parameters:
     * $this->serviceName: Plain Text to display when someone accesses this service 
     *               without the ?WSDL parameter in the URL. Whitespaces are
     *               removed from this and this is then used as the ID for the 
     *               XML in the WSDL. Please only use A-Z, 0-9 and spaces.
     *
     * Declare all Functions for this Web Service
     * $this->functions Array Parameters:
     *  funcName - Name of the particular function being served
     *  doc - Documentation to report from Web Service regarding this function
     *  inputParams - An array of arrays where name = name of field and type = data type
     *                  Omit if not required
     *  outputParams - As above, but for responses
     *                  Omit if not required
     *  soapAddress - The php file to send to to process SOAP requests                
     *****************************************************************************/


    // ----------------------------------------------------------------------------
    // END OF PARAMETERS SET UP
    // ----------------------------------------------------------------------------

    /*****************************************************************************
     * Process Page / Request
     *****************************************************************************/

        if (stristr($_SERVER['QUERY_STRING'], "wsdl")) {
            // WSDL request - output raw XML
                    header("Content-Type: application/soap+xml; charset=utf-8");
            echo $this->DisplayXML();
        } else {
            // Page accessed normally - output documentation
            $cp = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1); // Current page
            echo '<!-- Attention: To access via a SOAP client use ' . $cp . '?WSDL -->';
            echo '<html>';
            echo '<head><title>' . $this->serviceName . '</title></head>';
            echo '<body>';
            echo '<h1>' . $this->serviceName . '</h1>';
            echo '<p style="margin-left:20px;">To access via a SOAP client use <code>' . $this->serverpath . 'WSDL?WSDL</code></p>';

            // Document each function
            echo '<h2>Available Functions:</h2>';
            echo '<div style="margin-left:20px;">';
            for ($i=0;$i<count($this->functions);$i++) {
                echo '<h3>Function: ' . $this->functions[$i]['funcName'] . '</h3>';
                echo '<div style="margin-left:20px;">';
                echo '<p>';
                echo $this->functions[$i]['doc'];
                echo '<ul>';
                if (array_key_exists("inputParams", $this->functions[$i])) {
                    echo '<li>Input Parameters:<ul>';
                    /* for ($j=0;$j<count($this->functions[$i]['inputParams']);$j++) {
                            echo '<li>' . $this->functions[$i]['inputParams'][$j]['name'];
                            if (substr($this->functions[$i]['inputParams'][$j]['type'],0,4) == 'tns:') {
                                $subtype = substr($this->functions[$i]['inputParams'][$j]['type'],4); 
                                echo '(array)';
                                echo '<ul>';
                                for ($n=0;$n<count($this->functions[$i][$subtype]);$n++) {
                                    echo '<li>' . $this->functions[$i][$subtype][$n]['name'];
                                    echo ' (' . $this->functions[$i][$subtype][$n]['type'];
                                    echo ')';
                                    echo '</li>';
                                }
                                echo '</ul>';
                            } else {
                                echo ' (' . $this->functions[$i]['inputParams'][$j]['type'];
                                echo ')';
                            }
                            echo '</li>';
                    }*/
                    
                    echo  $this->DocumentArray($i,'inputParams');
                    echo '</ul></li>';
                }
                if (array_key_exists("outputParams", $this->functions[$i])) {
                    echo '<li>Output Parameters:<ul>';
                    echo  $this->DocumentArray($i,'outputParams');
                    /*for ($j=0;$j<count($this->functions[$i]['outputParams']);$j++) {
                        echo '<li>' . $this->functions[$i]['outputParams'][$j]['name'];
                        echo ' (' . $this->functions[$i]['outputParams'][$j]['type'];
                        echo ')</li>';
                    }*/
                    echo '</ul></li>';
                }
                echo '</ul>';
                echo '</p>';
                echo '</div>';
            }
            echo '</div>';

            echo '<h2>WSDL output:</h2>';
            echo '<pre style="margin-left:20px;width:800px;overflow-x:scroll;border:1px solid black;padding:10px;background-color:#D3D3D3;">';
            echo $this->DisplayXML(false);
            echo '</pre>';
            echo '</body></html>';
        }

        exit; 
    }
    
    private function DocumentArray($i,$item) {
        $html = "";
        if (array_key_exists($item, $this->functions[$i])) {
            $key = $this->functions[$i][$item];
        } else {
            $key = $this->sharedtns[$item];
        }
        for ($j=0;$j<count($key);$j++) {
                $html.= '<li>' . $key[$j]['name']; 
                if (substr($key[$j]['type'],0,4) == 'tns:') {
                    $subtype = substr($key[$j]['type'],4); 
                    $html.='(array)';
                    $html.= '<ul>';
                    $html.= $this->DocumentArray($i, $subtype);
                    $html.= '</ul>';
                } else {
                    $html.= ' (' . $key[$j]['type'];
                    $html.= ')';
                }
                $html.= '</li>';
        }
        return ($html);
    }

    /*****************************************************************************
     * Create WSDL XML 
     * @PARAM xmlformat=true - Display output in HTML friendly format if set false
     *****************************************************************************/
    private function DisplayXML($xmlformat=true) {

            $i = 0;                    // For traversing functions array
            $j = 0;                    // For traversing parameters arrays
            $str = '';                 // XML String to output

            // Tab spacings
            $t1 = '    ';
            if (!$xmlformat) $t1 = '&nbsp;&nbsp;&nbsp;&nbsp;';
            $t2 = $t1 . $t1;
            $t3 = $t2 . $t1;
            $t4 = $t3 . $t1;
            $t5 = $t4 . $t1;

            $serviceID = str_replace(" ", "", $this->serviceName);

            // Declare XML format
        $str .= '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n\n";

        // Declare definitions / namespaces
        $str .= '<wsdl:definitions ' . "\n"; 
        $str .= $t1 . 'xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" ' . "\n";
        $str .= $t1 . 'xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" ' . "\n";
        $str .= $t1 . 'xmlns:s="http://www.w3.org/2001/XMLSchema" ' . "\n"; 
        $str .= $t1 . 'targetNamespace="'.$this->serverpath.'" ' . "\n";
        $str .= $t1 . 'xmlns:tns="'.$this->serverpath.'" ' . "\n"; 
        $str .= $t1 . 'name="' . $serviceID . '" ' . "\n";
        $str .= '>' . "\n\n";

            // Declare Types / Schema
        $str .= '<wsdl:types>' . "\n";
        $str .= $t1 . '<s:schema elementFormDefault="qualified" targetNamespace="'.$this->serverpath.'">' . "\n";
        
        for ($i=0;$i<count($this->functions);$i++) {
            // Define Request Types
            if (array_key_exists("inputParams", $this->functions[$i])) {
               $str .= $t2 . '<s:element name="' . $this->functions[$i]['funcName'] . 'Request">' . "\n";
               $str .= $t3 . '<s:complexType><s:sequence>' . "\n";
               for ($j=0;$j<count($this->functions[$i]['inputParams']);$j++) {
                   $min = (isset($this->functions[$i]['inputParams'][$j]['min']) ? $this->functions[$i]['inputParams'][$j]['min'] : 0);
                   $max = (isset($this->functions[$i]['inputParams'][$j]['max']) ? $this->functions[$i]['inputParams'][$j]['max'] : 1);
                   $str .= $t4 . '<s:element minOccurs="'.$min.'" maxOccurs="'.$max.'" ';
                   $str .= 'name="' . $this->functions[$i]['inputParams'][$j]['name'] . '" ';
                   $type = $this->functions[$i]['inputParams'][$j]['type'] ;
                   if (substr($type,0,4) == 'tns:') {
                       $str .= 'type="' . $type . '" />' . "\n"; 
                   } else {
                       $str .= 'type="s:' . $type . '" />' . "\n";	
                   } 
               }
               $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
               $str .= $t2 . '</s:element>' . "\n";
            }
            // Define Response Types
            if (array_key_exists("outputParams", $this->functions[$i])) {
               $str .= $t2 . '<s:element name="' . $this->functions[$i]['funcName'] . 'Response">' . "\n";
               $str .= $t3 . '<s:complexType><s:sequence>' . "\n";
               for ($j=0;$j<count($this->functions[$i]['outputParams']);$j++) {
                   $min = (isset($this->functions[$i]['outputParams'][$j]['min']) ? $this->functions[$i]['outputParams'][$j]['min'] : 0);
                   $max = (isset($this->functions[$i]['outputParams'][$j]['max']) ? $this->functions[$i]['outputParams'][$j]['max'] : 1);
                   $str .= $t4 . '<s:element minOccurs="'.$min.'" maxOccurs="'.$max.'" ';
                   $str .= 'name="' . $this->functions[$i]['outputParams'][$j]['name'] . '" ';
                   $type = $this->functions[$i]['outputParams'][$j]['type'] ;
                   if (substr($type,0,4) == 'tns:') {
                       $str .= 'type="' . $type . '" />' . "\n"; 
                   } else {
                       $str .= 'type="s:' . $type . '" />' . "\n";	
                   }    
               }
               $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
               $str .= $t2 . '</s:element>' . "\n";
            }    	
        }
        // Shared tns
        foreach($this->sharedtns as $key => $arr) { 
            /* Deal with sun types within the input parameter */  
                 $str .= $t2 . '<s:complexType name="' . $key . '">' . "\n";
                 $str .= $t3 . '<s:sequence>' . "\n"; 
                 for ($j=0;$j<count($arr);$j++) {
                     $min = (isset($arr[$j]['min']) ? $arr[$j]['min'] : 0);
                     $max = (isset($arr[$j]['max']) ? $arr[$j]['max'] : 1);
                     $str .= $t4 . '<s:element minOccurs="'.$min.'" maxOccurs="'.$max.'" ';
                     $str .= 'name="' . $arr[$j]['name'] . '" ';
                     $type = $arr[$j]['type'] ;
                     if (substr($type,0,4) == 'tns:') {
                         $str .= 'type="' . $type . '" />' . "\n";
                     } else {
                         $str .= 'type="s:' . $type . '" />' . "\n";	
                     }
                 }
                 $str .= $t3 . '</s:sequence>' . "\n";
                 $str .= $t2 . '</s:complexType>' . "\n";

        }
        $str .= $t1 . '</s:schema>' . "\n";
        $str .= '</wsdl:types>' . "\n\n";

        // Declare Messages
        for ($i=0;$i<count($this->functions);$i++) {
            // Define Request Messages
            if (array_key_exists("inputParams", $this->functions[$i])) {
                    $str .= '<wsdl:message name="' . $this->functions[$i]['funcName'] . 'Request">' . "\n";
                $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $this->functions[$i]['funcName'] . 'Request" />' . "\n";
                $str .= '</wsdl:message>' . "\n";
            }
            // Define Response Messages
            if (array_key_exists("outputParams", $this->functions[$i])) {
                $str .= '<wsdl:message name="' . $this->functions[$i]['funcName'] . 'Response">' . "\n";
                $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $this->functions[$i]['funcName'] . 'Response" />' . "\n";
                $str .= '</wsdl:message>' . "\n\n";
            }
        }

        // Declare Port Types
        for ($i=0;$i<count($this->functions);$i++) {
            $str .= '<wsdl:portType name="' . $this->functions[$i]['funcName'] . 'PortType">' . "\n";
            $str .= $t1 . '<wsdl:operation name="' . $this->functions[$i]['funcName'] . '">' . "\n";
            if (array_key_exists("inputParams", $this->functions[$i])) 
               $str .= $t2 . '<wsdl:input message="tns:' . $this->functions[$i]['funcName'] . 'Request" />' . "\n";
            if (array_key_exists("outputParams", $this->functions[$i])) 
               $str .= $t2 . '<wsdl:output message="tns:' . $this->functions[$i]['funcName'] . 'Response" />' . "\n";
            $str .= $t1 . '</wsdl:operation>' . "\n";
            $str .= '</wsdl:portType>' . "\n\n";
        }

        // Declare Bindings
        for ($i=0;$i<count($this->functions);$i++) {
            $str .= '<wsdl:binding name="' . $this->functions[$i]['funcName'] . 'Binding" type="tns:' . $this->functions[$i]['funcName'] . 'PortType">' . "\n";
            $str .= $t1 . '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />' . "\n";
            $str .= $t1 . '<wsdl:operation name="' . $this->functions[$i]['funcName'] . '">' . "\n";
            $str .= $t2 . '<soap:operation soapAction="' . $this->functions[$i]['soapAddress'] . '#' . $this->functions[$i]['funcName'] . '" style="document" />' . "\n";
            if (array_key_exists("inputParams", $this->functions[$i]))
                $str .= $t2 . '<wsdl:input><soap:body use="literal" /></wsdl:input>' . "\n";
            if (array_key_exists("outputParams", $this->functions[$i]))
                $str .= $t2 . '<wsdl:output><soap:body use="literal" /></wsdl:output>' . "\n";
            $str .= $t2 . '<wsdl:documentation>' . $this->functions[$i]['doc'] . '</wsdl:documentation>' . "\n";
            $str .= $t1 . '</wsdl:operation>' . "\n";
            $str .= '</wsdl:binding>' . "\n\n";
        }

        // Declare Service
        $str .= '<wsdl:service name="' . $serviceID . '">' . "\n";
        for ($i=0;$i<count($this->functions);$i++) {
            $str .= $t1 . '<wsdl:port name="' . $this->functions[$i]['funcName'] . 'Port" binding="tns:' . $this->functions[$i]['funcName'] . 'Binding">' . "\n";
            $str .= $t2 . '<soap:address location="' . $this->functions[$i]['soapAddress'] . '" />' . "\n";
            $str .= $t1 . '</wsdl:port>' . "\n";
        }
        $str .= '</wsdl:service>' . "\n\n";
        
       /* $str .= "
            <wsdl:service name='putReservationService'>
                <wsdl:port name='putReservationPort' binding='tns:putReservationBinding'>
                  <soap:address location='https://www.skylinecms.co.uk/beta/OneTouchSoap'/>
                </wsdl:port>
              </wsdl:service>
                ";
        
        $str .= "
            <wsdl:service name='getAvailabilityService'>
              <wsdl:port name='getAvailabilityPort' binding='tns:getAvailabilityBinding'>
                  <soap:address location='https://www.skylinecms.co.uk/beta/OneTouchSoap'/>
                </wsdl:port>
              </wsdl:service>
                ";*/

        // End Document
        $str .= '</wsdl:definitions>' . "\n";

        if (!$xmlformat) $str = str_replace("<", "&lt;", $str); 
        if (!$xmlformat) $str = str_replace(">", "&gt;", $str);
        if (!$xmlformat) $str = str_replace("\n", "<br />", $str);
        return $str;
    }

}

?>
