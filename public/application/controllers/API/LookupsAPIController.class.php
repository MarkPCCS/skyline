<?php
/**
 * LookupsAPIController.class.php
 * 
 * Implementation of Lookups API for SkyLine
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 01/06/2012  1.00    Andrew J. Williams    Initial Version
 ********************************************************************************/

require_once('CustomRESTController.class.php');
include_once(APPLICATION_PATH.'/controllers/API/SkylineAPI.class.php');

class LookupsAPI extends SkylineAPI {
        
   
    /**
     * Description
     * 
     * Deal with get  
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    
    public function get( $args ) {
        $api_lookups_model = $this->loadModel('APILookups');      
        
        $rec_accounts = $api_lookups_model->getLookups($this->user->UserID);
        $rec_unit_types = $api_lookups_model->getUnitTypes($this->user->UserID);
        $rec_service_types = $api_lookups_model->getServiceTypes($this->user->UserID);
        $rec_manufacturers = $api_lookups_model->getManufacturers($this->user->UserID);
        
        $response = array('Lookups' => $rec_accounts );
        $response['Lookups']['UnitTypes'] = $this->recordSetSerialise($rec_unit_types,'UnitType');
        $response['Lookups']['ServiceTypes'] = $this->recordSetSerialise($rec_service_types,'ServiceType');
        $response['Lookups']['Manufacturers'] = $rec_manufacturers;
        
        $this->sendResponse(200, $response);

    }
        
}

?>