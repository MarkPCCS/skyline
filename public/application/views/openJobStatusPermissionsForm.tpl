
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
    <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
        <fieldset>
            <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
            <p>

                <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

            </p>

            <p>

                <span class= "bottomButtons" >
                    <input type="submit" name="cancel_btn" 
                           class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"
                           value="{$page['Buttons']['ok']|escape:'html'}" >

                </span>

            </p>


        </fieldset>   
    </form>            


    </div>    
    
{else}  

    <script>
        function clearSearch(){        
        $('#search_permission').val("");
        $("#search_results_test_filter input").val("");
        $("#search_results_test_filter input").trigger("keyup"); 
        }
        
        
        function selectRow(nRow, aData){
        
           
        
            //console.info(aData[3]);
            
            if(aData[3] == null){
                $('td:eq(2)', nRow).html( '<input name="StatusID_'+aData[0]+'" type="checkbox" />' );
            }else{
                $('td:eq(2)', nRow).html( '<input name="StatusID_'+aData[0]+'" value="selected" type="checkbox" checked="checked" />' );
            }
       
        }
        
     $(document).ready(function(){
     
        $("#search_permission").keyup(function() {
                                        var searchval=$('#search_permission').val();
                                        //alert(searchval);
                                        chkValue=$('input[name=PermissionTagged]:radio:checked').val();                                       
                                        if(chkValue) searchval=searchval+" "+chkValue;
                                        $("#search_results_test_filter input").val(searchval);
                                        $("#search_results_test_filter input").trigger("keyup");                                        
                                      });

        $( "#InactivityTimeout" ).spinner({
                                    min: 1,
                                    max: 9999                                 
                                  }).change( function() {
                                    alert('change event '+$(this).spinner('value'));
                                        if ( $(this).spinner('value') > 9999 ) {
                                            $( this ).spinner( 'value', 9999 );
                                        } else if ( $(this).spinner('value') < 1 ) {
                                            $( this ).spinner( 'value', 1 );
                                        }
                                    });
                                    
                                    

       
                                    
        userRolesDTTable("{$PermissionTagged}");
        
        $("input[name='PermissionTagged']").change(function(){
                    
                    // userRolesDTTable($(this).val());
                    $('#search_permission').val("");
                    
                    if($(this).val()!='')
                    {
                        $filterText = '"'+$(this).val()+'"';
                    }    
                    else
                    {
                        $filterText = '';
                    }
                   
                    
                    $("#search_results_test_filter input").val($filterText);
                    $("#search_results_test_filter input").trigger("keyup");
                    
                });
        
        
         
         
          
        $(document).on('click', '.DTCheckBox', function() { 

            var $chk_name = $(this).attr("name");

            var $ticked = false;
            
            if($(this).is(':checked'))
            {
                    $("#Hidden"+$chk_name).val('Active');
                    $(this).val("Tagged");
                    $ticked = true;
            }
            else
            {
                $("#Hidden"+$chk_name).val('In-active');
                $(this).val("Un-tagged");
            }
            
             countCheckBoxes();
             
             
              var data = oTable['search_results_test'].fnGetNodes();
              
              for(var i = 0; i < data.length; i++) {
                var el = $($(data[i]).find("td")[1]).find("input");
                
                
                if(el) 
                {
                    if($chk_name==el.attr("name"))
                    {    
                        $RowIndex = el.attr("id").replace('PermissionID_RowIndex_', '');    
                       // alert($RowIndex);    
                        if($ticked)
                        {
                            $html_check_box = '<input class="DTCheckBox" id="'+el.attr("id")+'" name="'+el.attr("name")+'" value="Tagged" type="checkbox" checked="checked" />';
                        }
                        else
                        {
                           $html_check_box = '<input class="DTCheckBox" id="'+el.attr("id")+'" name="'+el.attr("name")+'" type="checkbox" value="Un-tagged" />';
                        }    
                        
                        oTable['search_results_test'].fnUpdate( $html_check_box, $RowIndex, 2 ) ; 
                        
                        break;
                    }
                   
                }
             }
             
           
           return false;
           
            
        });
         
         function countCheckBoxes()
         {
             var $TaggedCount   = 0;
             var $UnTaggedCount = 0;
             
             $('.HiddenPermissions').each(function() {
                                   
                                   
                                           
                               if($(this).val()=='Active')
                                   {
                                       $TaggedCount++;
                                   }
                               else
                                   {
                                        $UnTaggedCount++;
                                   }
                                        
                                        
                                    } );
                                    
                                    
            $("#TaggedPermissions").html($TaggedCount);                        
            $("#UntaggedPermissions").html($UnTaggedCount);
            $("#AllPermissions").html($TaggedCount+$UnTaggedCount);
             
         }
         
         
         $("#search_results_test_filter").hide();
         
         

       
    }); 
    
    
    function userRolesDTTable(TaggedFlag)
    {
        $('#search_results_test').PCCSDataTable( {            
            
            oLanguage:          { "sEmptyTable": "No Permissions" },
            bServerSide:        false,
            htmlTableId:        'search_results_test',
            htmlTablePageId:    'search_results_test_panel',
            fetchDataUrl:       '{$_subdomain}/Data/OpenJobsStatusPermission/{$entitytype}/{$datarow.ID}/1/'+TaggedFlag+'/',
            aoColumns: [ 
                    { 'bVisible' : false, "bSearchable": false  },
                    { 'bSortable' : true, "bSearchable": true, "sWidth" : "450px" },
                    { 'bSortable' : false, "sWidth" : "80px" }
            ],            
            iDisplayLength:  50,        
           // fnRowCallback:          'selectRow',
            bottomButtonsDivId:'dataTables_child',
            searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',      
            //tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
            aaSorting:	    [[ 1, "asc" ]],
           // sScrollY: '200px',
           // bScrollCollapse: false
            //bPaginate: false
            bDestroy:true      

        }); 
    }
    
    </script>
    
    <div id="serviceTypesFormPanel" class="SystemAdminFormPanel" >
        <form id="SSPForm" name="SSPForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" >{$form_legend|escape:'html'}</legend>

                <p>
                {$page['Text'][$entityType]|escape:'html'}: {$datarow.Name}

                </p>

                
                <div class="clear" style="float:right">
                    <label style="font-size: 1.0em;margin: 3px 0px 0px 0px;">Search within results:  
                        <span><input type="text" id="search_permission" />
                            <img onclick="clearSearch();" src="/css/Skins/skyline/images/close.png" style="position: absolute;
                                                                                        right: 4px;
                                                                                        top :6px;
                                                                                        cursor: pointer;" />
                        </span>
                    </label>
                </div>
                
               
                <p></p>
                {if $datarow.ID neq '' && $datarow.ID neq '0'}
                    <div style="float:right;">
                        <input name="PermissionTagged" type="radio" value="Tagged" {if $PermissionTagged eq 'Active'} checked="checked" {/if} > {$page['Text']['tagged']|escape:'html'} (<span id="TaggedPermissions" >{$Tagged}</span>)
                        <input name="PermissionTagged" type="radio" value="Un-tagged" {if $PermissionTagged eq 'In-active'} checked="checked" {/if} > {$page['Text']['untagged']|escape:'html'} (<span id="UntaggedPermissions" >{$Untagged}</span>)
                        <input name="PermissionTagged" type="radio" value="" {if $PermissionTagged eq 'Both'} checked="checked" {/if} > {$page['Text']['both']|escape:'html'} (<span id="AllPermissions" >{$Both}</span>)
                    </div>
                {/if}

                <div class="LTResultsPanel" id="search_results_test_panel" >
                <table id="search_results_test" border="0" cellpadding="0" cellspacing="0" class="browse">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="width:450px"  >{$page['Labels']['status_permission']|escape:'html'}</th>
                            <th style="width:80px;"  >{$page['Labels']['select']|escape:'html'}</th>

                        </tr>
                    </thead>
                    <tbody></tbody>
                </table> 
                </div>     
                            <br />
                <p>

                    <span class= "bottomButtons" >

                        {foreach $TotalPermissions as $tp}

                             {if $tp.2}

                                 <input class="HiddenPermissions" id="HiddenPermissionID_{$tp.0}" name="HiddenPermissionID_{$tp.0}"  type="hidden" value="Active" />

                             {else}

                                 <input class="HiddenPermissions" id="HiddenPermissionID_{$tp.0}" name="HiddenPermissionID_{$tp.0}" type="hidden" value="In-active" />

                             {/if}    

                        {/foreach} 

                        <input type="hidden" name="ID"  value="{$datarow.ID}" >
                        <input type="hidden" name="entitytype"  value="{$entitytype}" >

                            <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >


                          
                            <input type="submit" name="cancel_btn" class="textSubmitButton centerBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                    </span>

                </p>

            </fieldset>    

        </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();" value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>              
    
    
</div>                             