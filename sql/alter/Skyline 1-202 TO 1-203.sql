# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.202');

# ---------------------------------------------------------------------- #
# Modify table "sp_engineer_day_location"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE `sp_engineer_day_location`
	CHANGE COLUMN `DateModified` `DateModified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `Longitude`;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.203');
