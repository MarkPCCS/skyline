# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.261');

# ---------------------------------------------------------------------- #
# Modify Table contact_history_action                                    #
# ---------------------------------------------------------------------- # 
ALTER TABLE `contact_history_action`
	ADD COLUMN `Source` ENUM('Contact History','Remote Engineer') NOT NULL DEFAULT 'Contact History' AFTER `Private`;


# ---------------------------------------------------------------------- #
# Modify Table contact_history                                           #
# ---------------------------------------------------------------------- # 	
ALTER TABLE `contact_history`
	ADD COLUMN `ImageURL` VARCHAR(50) NULL DEFAULT NULL AFTER `Private`,
	ADD COLUMN `ThumbnailURL` VARCHAR(50) NULL DEFAULT NULL AFTER `ImageURL`;

	
# ---------------------------------------------------------------------- #
# Modify Table sp_part_stock_template                                    #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_part_stock_template ADD COLUMN DefaultServiceProviderSupplierID INT(10) NULL COMMENT 'Primary supplier' AFTER ServiceProviderID;


# ---------------------------------------------------------------------- #
# Insert into  Table sp_part_status                                      #
# ---------------------------------------------------------------------- # 
INSERT IGNORE INTO sp_part_status (SPPartStatusID, Description, Available, InStock) VALUES (1, '00 An initial stock import', 'Y', 'Y'), (2, '01 Part Order being received', 'Y', 'Y'), (3, '03 Part Fitted', 'N', 'N'), (4, '04 Part Sold', 'N', 'N'), (5, '05 Part Reserved', 'N', 'Y'), (6, '06 Part Removed From Job', 'Y', 'Y'), (7, '07 Part Waiting Order', 'N', 'N'), (8, '08 Part Ordered', 'N', 'N');


# ---------------------------------------------------------------------- #
# Insert into  Table permission                                      #
# ---------------------------------------------------------------------- # 
INSERT INTO permission (PermissionID,  Name, Description) values (9001, 'Data Integrity - Bulk Refresh', 'Allow the user to run the Servicebase bulk refresh');


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.262');
