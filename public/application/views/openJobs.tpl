{extends "DemoLayout.tpl"}

{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $OpenJobsPage}
    {$fullscreen=true}
{/block}


{block name=afterJqueryUI}
 
<script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>

<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/ZeroClipboard.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/columnfilter.js"></script>


<link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />    
<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
   
<style>
    .scroll-pane { 
	overflow: auto; 
	width: 845px; 
	float:left; 
	background-color:transparent;
	background-image: url('');
	border-style:none; 
    }
    .scroll-content { 
	width: {$contentWidth}px; 
	float: left; 
	background-color:transparent;
	background-image: url('');
	border-style:none; 
    }
    .scroll-content-item { 
	width: 100px; 
	height: 100px; 
	float: left; 
	margin: 10px; 
	font-size: 3em; 
	line-height: 96px; 
	text-align: center; 
    }
    * html .scroll-content-item { 
	display: inline; 
    } /* IE6 float double margin bug */
    .scroll-bar-wrap { 
	clear: left; 
	padding: 5px 4px 0 2px; 
	margin: 0 -1px -1px -1px; 
	border-style:none;   
    }
    .scroll-bar-wrap .ui-slider { 
	background: none; 
	border:0; 
	height: 2em; 
	margin: 0 auto;  
    }
    .scroll-bar-wrap .ui-handle-helper-parent { 
	position: relative; 
	width: 100%; 
	height: 100%; 
	margin: 0 auto; 
    }
    .scroll-bar-wrap .ui-slider-handle { 
	top:.2em; 
	height: 1.5em; 
    }
    .scroll-bar-wrap .ui-slider-handle .ui-icon { 
	margin: -8px auto 0; 
	position: relative; 
	top: 50%; 
    }
    .search_init, .text_filter, .text_filter:focus{
        width:0px;
       
background: none;
border: 0px solid  transparent!important;
color: white;
font-weight: bold;
outline: none;
-webkit-box-shadow: none!important;
box-shadow: none!important;
font-size:11px!important;
padding-left: 0;
padding-right: 0;
        }
</style>
   
 <script type="text/javascript">
        $.fn.jPicker.defaults.images.clientPath = "{$_subdomain}/css/themes/pccs/images/colorPicker/";
 </script>
   
{/block}



{block name=scripts}

    
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script type="text/javascript">
      
    var table;
      
    //It opens Job update page.
    function openJob($rowData){
	if($rowData[0])
	document.location.href = "{$_subdomain}/index/jobupdate/"+urlencode($rowData[0]); 
    }
      
    function save_dt_view (oSettings, oData) {
        localStorage.setItem( 'DataTables_'+window.location.pathname, JSON.stringify(oData) );
    }

    function load_dt_view (oSettings) {
      return JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname) );
    }
               
    $(document).ready(function() {
             
             
    
    
        $('#openjobstabs').tabs();
    
    $('.showalljobs').click(function(event){
            event.preventDefault();
            $(this).attr("href", ($(this).attr('href') +"/showall/ojvBy="+$('input[name=ojvBy]:checked').val()));
            document.location.href =  $(this).attr("href");
    });
    
    
    $(window).resize(function() {   $('.scroll-pane').width($(window).width()-100);});
      //adjusting scroller width
      
      $('.scroll-pane').width($(window).width()-100);
      //end
	$(".TimeSlotActiveTab").removeClass("TimeSlotActiveTab");
	$("#Tab"+$("#PreferredClientGroupTabID").val()+" a").addClass("TimeSlotActiveTab");
        
   
	$(document).on('click', "#insert_save_btn", function() { 
	    $("#insert_save_btn").hide();
	    $("#cancel_btn").hide();
	    $("#processDisplayText").show();
	    
	    $.post("{$_subdomain}/Data/updateUserStatusPreferences/Page=openJobs/Type=js",        
		    $("#StatusPreferencesForm").serialize(), 
		    function(data) {
			//var p = eval("(" + data + ")");
			document.location.href = "{$_subdomain}/Job/openJobs";
		    }
	    ); //Post ends here...

	    return false;

       }); 


	$(document).on('click', "#insert_save_btn2", function() { 

	    $("#insert_save_btn2").hide();
	    $("#cancel_btn2").hide();
	    $("#processDisplayText2").show();

	    $.post("{$_subdomain}/Data/updatePreferentialClientsManufacturers/Page=openJobs",        
		    $("#PreferentialForm").serialize(),      
		    function(data) {
			//var p = eval("(" + data + ")");
			document.location.href = "{$_subdomain}/Job/openJobs";
		    }
	    ); //Post ends here...

	    return false;

	}); 
        
	$(document).on('click', "#hideNonSbJobs", function() { 

        $.post("{$_subdomain}/Job/hideNonSbJobs",{ hideNonSbJobs:$('#hideNonSbJobs').attr('checked')},     
		    function(data) {
                    
			table=$("#openJobsResults").dataTable();
                        table.fnReloadAjax('{$_subdomain}/Job/openJobsResult' + brandParam + sTypeParam + scTypeParam + manufacturerParam + clientParam + serviceProviderParam + ojByParam + ojvByParam + btnNameParam + btnValueParam + btnValue2Param+'/');
                       
		    }
	    ); //Post ends here...

	    

	}); 
        
        
        
        
        $(document).on('keyup', "#Button1", function() { 
        
                $("#Button1_1").val($("#Button1").val());
                
        });
        $(document).on('keyup', "#Button3", function() { 
        
                $("#Button3_1").val($("#Button3").val());
                
        });
        
        
        
        $(document).on('click', ".TatButton", function() { 
        
                if($(this).attr("name")=="tat_button1")
                {
                     document.location.href = '{$_subdomain}/Job/openJobs' + '/client=' + urlencode({$client}) + '/manufacturer=' + urlencode({$manufacturer}) + '/brand=' + urlencode({$brand}) + '/branch=' + urlencode({$branch})  + '/client=' + urlencode({$client})  + '/unitType=' + urlencode({$unitType}) + '/skillSet=' + urlencode({$skillSet}) + '/ojBy=' + urlencode('{$ojBy}') + '/ojvBy=' + urlencode('{$ojvBy}') + '/serviceProvider=' + urlencode({$serviceProvider}) + '/sType=' + urlencode({$sType})+'/btnName=b1/btnValue='+urlencode('{$TatResult['Button1']}');   
                } 
                else  if($(this).attr("name")=="tat_button2")
                {
                     document.location.href = '{$_subdomain}/Job/openJobs' + '/client=' + urlencode({$client}) + '/manufacturer=' + urlencode({$manufacturer}) + '/brand=' + urlencode({$brand}) + '/branch=' + urlencode({$branch}) + '/client=' + urlencode({$client})  + '/unitType=' + urlencode({$unitType}) + '/skillSet=' + urlencode({$skillSet}) + '/ojBy=' + urlencode('{$ojBy}') + '/ojvBy=' + urlencode('{$ojvBy}') + '/serviceProvider=' + urlencode({$serviceProvider}) + '/sType=' + urlencode({$sType})+'/btnName=b2/btnValue='+urlencode('{$TatResult['Button1']}')+'/btnValue2='+urlencode('{$TatResult['Button3']}');   
                }  
                else if($(this).attr("name")=="tat_button3")
                {
                     document.location.href = '{$_subdomain}/Job/openJobs' + '/client=' + urlencode({$client}) + '/manufacturer=' + urlencode({$manufacturer}) + '/brand=' + urlencode({$brand}) + '/branch=' + urlencode({$branch}) + '/client=' + urlencode({$client})  +  '/unitType=' + urlencode({$unitType}) + '/skillSet=' + urlencode({$skillSet}) + '/ojBy=' + urlencode('{$ojBy}') + '/ojvBy=' + urlencode('{$ojvBy}') + '/serviceProvider=' + urlencode({$serviceProvider}) + '/sType=' + urlencode({$sType})+'/btnName=b3/btnValue='+urlencode('{$TatResult['Button3']}');   
                }
                else if($(this).attr("name")=="tat_button4")
                {
                     document.location.href = '{$_subdomain}/Job/openJobs' + '/client=' + urlencode({$client}) + '/manufacturer=' + urlencode({$manufacturer}) + '/brand=' + urlencode({$brand}) + '/branch=' + urlencode({$branch}) + '/client=' + urlencode({$client})  + '/unitType=' + urlencode({$unitType}) + '/skillSet=' + urlencode({$skillSet}) + '/ojBy=' + urlencode('{$ojBy}') + '/ojvBy=' + urlencode('{$ojvBy}') + '/serviceProvider=' + urlencode({$serviceProvider}) + '/sType=' + urlencode({$sType})+'/btnName=b4/btnValue='+urlencode('{$TatResult['Button4']}');   
                }
                
        });
        
        
        $(document).on('click', "#insert_save_btn3", function() { 


            if($("label.fieldError:visible").length>0)
            {

            }
            else
            {
                $("#insert_save_btn3").hide();
                $("#cancel_btn3").hide();
                $("#processDisplayText3").show();
            
            }


	   
            
            
            $('#TatSetupForm').validate({

                    ignore: '',
                                        
                    rules:  {
                        
                        Button1:
                        {
                            required: true,
                            digits: true
                        },
                        Button1Colour:
                        {
                            required: true
                        },
                        Button2Colour:
                        {
                            required: true
                        },        
                        Button3:
                        {
                            required: true,
                            digits: true
                        },
                        Button3Colour:
                        {
                            required: true
                        },
                        Button4:
                        {
                            required: true,
                            digits: true
                        },
                        Button4Colour:
                        {
                            required: true
                        }         
                        
                    },
                    messages: {
                                            
                        Button1:
                        {
                            required: "{$page['Errors']['button']|escape:'html'}"
                        },
                        Button1Colour:
                        {
                            required: "{$page['Errors']['colour']|escape:'html'}"
                        },
                        Button2Colour:
                        {
                            required: "{$page['Errors']['colour']|escape:'html'}"
                        },        
                        Button3:
                        {
                            required: "{$page['Errors']['button']|escape:'html'}"
                        },
                        Button3Colour:
                        {
                            required: "{$page['Errors']['colour']|escape:'html'}"
                        },
                        Button4:
                        {
                            required: "{$page['Errors']['button']|escape:'html'}"
                        },
                        Button4Colour:
                        {
                            required: "{$page['Errors']['colour']|escape:'html'}"
                        }         
                    },
                    errorPlacement: function(error, element) {
                 
                        
                            if(element.attr("name")=="Button1" || element.attr("name")=="Button1Colour")
                            {
                                 error.insertAfter( $("#Button1Error") );
                            }
                            else if(element.attr("name")=="Button2" || element.attr("name")=="Button2Colour")
                            {
                                 error.insertAfter( $("#Button2Error") );
                            }
                            else if(element.attr("name")=="Button3" || element.attr("name")=="Button3Colour")
                            {
                                 error.insertAfter( $("#Button3Error") );
                            }
                            else if(element.attr("name")=="Button4" || element.attr("name")=="Button4Colour")
                            {
                                 error.insertAfter( $("#Button4Error") );
                            }
                            else
                            {
                                 error.insertAfter( element );
                            }

                           
                            $("#insert_save_btn3").show();
                            $("#cancel_btn3").show();
                            $("#processDisplayText3").hide();

                 
                   },
                    errorClass: 'fieldError',
                    onkeyup: false,
                    onblur: false,
                    errorElement: 'label',

                    submitHandler: function() {

                        $.post("{$_subdomain}/LookupTables/ProcessData/TAT/",        
                                $("#TatSetupForm").serialize(),      
                                function(data) {

                                    document.location.href = "{$_subdomain}/Job/openJobs";
                                }
                        ); //Post ends here...
                    }

                    
            
            });
            
	   

	}); 
          

        $(document).on('click', "#cancel_btn3", function() { 
	    $("#insert_save_btn3").hide();
	    $("#cancel_btn3").hide();
	    $("#processDisplayText3").show();
	    document.location.href = "{$_subdomain}/Job/openJobs";
	}); 
      
	$(document).on('click', "#cancel_btn2", function() { 
	    $("#insert_save_btn2").hide();
	    $("#cancel_btn2").hide();
	    $("#processDisplayText2").show();
	    document.location.href = "{$_subdomain}/Job/openJobs";
	}); 
                
        
	$(document).on('click', "#cancel_btn", function() { 
	    $("#insert_save_btn").hide();
	    $("#cancel_btn").hide();
	    $("#processDisplayText").show();
	    document.location.href = "{$_subdomain}/Job/openJobs";
	});  
      
      
       {*
            Added javaScript function for clear,Display,check all and individual checked. to avoid same code written for 
            different elements. 
        *}
      
        clickcheckbox = function(strobj,idobj){
            if(idobj.is(':checked')){
                $(("#"+strobj+"Priority"+idobj.val())).show();
            }
            else{            
                 $(("#"+strobj+"Priority"+idobj.val())).hide();
                 if($(("#"+strobj+"DisplaySelected")).is(':checked')) {
		    idobj.parent().hide(); 
		}
            }
        };
        
        clearcheckbox = function(idobj){
            var elmnt = idobj.attr('id').replace("ClearAll","");
            $(("#"+elmnt+"DisplaySelected")).removeAttr("checked");
            $('.'+elmnt+'CheckBox').each(function () {
		$(this).removeAttr("checked"); 
                $(("#"+elmnt+"Priority"+$(this).val())).hide();
                $(this).parent().show();
	    });
	    return false;
        };
        
        tagcheckbox = function(idobj){
            var elmnt = idobj.attr('id').replace("TagAll","");
            $('.'+elmnt+'CheckBox').each(function () {
		$(this).attr("checked", true); 
		$(("#"+elmnt+"Priority"+$(this).val())).show();
		$(this).parent().show();
	    });
	    return false;
        };
        
        selectedcheckbox = function(idobj){
            var elmnt = idobj.attr('id').replace("DisplaySelected","");
            if(idobj.is(':checked')) {
		$('.'+elmnt+'CheckBox').each(function() {
		    if(!this.checked) {
			$(this).parent().hide(); 
		    }
		 });
	    } else {
		$('.'+elmnt+'CheckBox').each(function() {
		    $(this).parent().show(); 
		});
	    }
        };
        
	
         //Status       
	$(document).on('click', ".StatusCheckBox", function() { 
	    clickcheckbox('Status',$(this));
	});
        
        //ServiceProvider
	$(document).on('click', ".ServiceProviderCheckBox", function() { 
	    clickcheckbox('ServiceProvider',$(this));
	});
      
        //Manufacturer
	$(document).on('click', ".ManufacturerCheckBox", function() { 
            clickcheckbox('Manufacturer',$(this)); 
	});
        
        //Network
        $(document).on('click', ".NetworkCheckBox", function() {
            clickcheckbox('Network',$(this));
	});  
        
        //Branch
	$(document).on('click', ".BranchCheckBox", function() {
            clickcheckbox('Branch',$(this));
	});  

        //brand
	$(document).on('click', ".BrandCheckBox", function() { 
	    clickcheckbox('Brand',$(this));
	});
        
        //client
	$(document).on('click', ".ClientCheckBox", function() { 
	    clickcheckbox('Client',$(this));
	});
        
        //Unit Types
	$(document).on('click', ".UnitTypeCheckBox", function() { 
	    clickcheckbox('UnitType',$(this));
	});
        
        //Skillset
	$(document).on('click', ".SkillSetCheckBox", function() { 
	    clickcheckbox('SkillSet',$(this));
	});


        $('#ManufacturerTagAll,#NetworkTagAll,#StatusTagAll,#BranchTagAll,#BrandTagAll,#ServiceProviderTagAll,#ClientTagAll,#UnitTypeTagAll,#SkillSetTagAll').click(function(){
            tagcheckbox($(this));
        });
        $('#ManufacturerDisplaySelected,#NetworkDisplaySelected,#StatusDisplaySelected,#BranchDisplaySelected,#BrandDisplaySelected,#ServiceProviderDisplaySelected,#ClientDisplaySelected,#UnitTypeDisplaySelected,#SkillSetDisplaySelected').click(function(){
            selectedcheckbox($(this));
        });
        $('#ManufacturerClearAll,#NetworkClearAll,#StatusClearAll,#BranchClearAll,#BrandClearAll,#ServiceProviderClearAll,#ClientClearAll,#UnitTypeClearAll,#SkillSetClearAll').click(function(){
            clearcheckbox($(this));
        });
        
        {*
            End of javascript function
        *} 
   
	$(document).on('click', "#StatusPreferences", function(event) { 
            event.preventDefault();
	    //It opens color box popup page.              
	    $.colorbox({   
		inline:		true,
		href:		"#DivStatusPreferences",
		title:		'',
		opacity:	0.75,
		height:		940,
		width:		720,
		overlayClose:	false,
		escKey:		false,
		onLoad: function() {
		    //$('#cboxClose').remove();
		},
		onClosed: function() {
		    //location.href = "#EQ7";
		},
		onComplete: function() {
		    $.colorbox.resize();
		}
	    }); 
	});
      
        $(document).on('click', "#RefreshStatusPreferences", function(event) { 
            event.preventDefault();
            window.location.href = '{$_subdomain}/Job/openJobs/sType=0';
	});
      
       $(document).on('click', "#tatSetup", function() { 
	    //It opens color box popup page.              
	    $.colorbox({   
		inline:		true,
		href:		"#DivTatSetup",
		title:		'',
		opacity:	0.75,
		height:		940,
		width:		720,
		overlayClose:	false,
		escKey:		false,
		onLoad: function() {
		    //$('#cboxClose').remove();
		},
		onClosed: function() {
		    //location.href = "#EQ7";
		},
		onComplete: function() {
		    $.colorbox.resize();
		}
	    }); 
	});
        
        
        for($i=1;$i<=4;$i++)
        {
            $('#Button'+$i+'Colour').jPicker( {
                    window:
                    {
                      expandable: true,
                                  position:
                                  {
                                    x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
                                    y: '150px' // acceptable values "top", "bottom", "center", or relative px value
                                  }
                    }
                  }

              );
         } 

        $(".Color").css({ width: "25px", height: "24px", padding: "0px" });

        $(document).on('click', ".ojByElement", function() {
            document.location.href = '{$_subdomain}/Job/openJobs' + '/client=' + urlencode({$client}) + '/unitType='+ urlencode({$unitType}) + '/manufacturer=' + urlencode({$manufacturer}) + '/brand=' + urlencode({$brand}) + '/branch=' + urlencode({$branch})   + '/ojBy=' + urlencode($(this).val()) + '/serviceProvider=' + urlencode({$serviceProvider}) + '/sType=' + urlencode({$sType}) + '/ojvBy=' + urlencode('{$ojvBy}');
        });
        
        
        $(document).on('click', ".ojvByElement", function() {
{*            document.location.href = '{$_subdomain}/Job/openJobs' + '/client=' + urlencode({$client}) + '/unitType='+ urlencode({$unitType}) + '/manufacturer=' + urlencode({$manufacturer}) + '/brand=' + urlencode({$brand}) + '/branch=' + urlencode({$branch})   + '/ojBy=' + urlencode($(this).val()) + '/serviceProvider=' + urlencode({$serviceProvider}) + '/sType=' + urlencode({$sType}) + '/ojvBy=' + urlencode($(this).val());*}
            document.location.href = '{$_subdomain}/Job/openJobs' + '/ojvBy=' + urlencode($(this).val());
        });
        
        
        
        //click handler for help icons starts here.
        $(document).on('click', '.helpTextIcon', 
                                function() {
    
      
                $HelpTextCode =  $(this).attr("id");
      
                //It opens color box popup page.              
                $.colorbox( {   href: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/' + Math.random(),
                                title: "{$page['Text']['help']|escape:'html'}",
                                opacity: 0.75,
                                width:750,
                                overlayClose: false,
                                escKey: false, 
                                onComplete: function() {
                                    $(this).colorbox.resize({ height: ($('#HelpPageDiv').height() + 150) + "px" });
                                },
                                onClosed: function() {
      
                                }
                }); 


                return false;                
        });
        //click handler for help icons ends here.  
        
        
        
      
      
      
	$(document).on('click', "#PreferentialClientsManufacturers", function(event) { 
            event.preventDefault();
	    //It opens color box popup page.              
	    $.colorbox({   
		inline:		true,
		href:		"#DivPreferential",
		title:		'',
		opacity:	0.75,
		height:		700,
		width:		900,
		overlayClose:	false,
		escKey:		false,
		onComplete: function() {
		    $.colorbox.resize();
		},
		onLoad: function() {
		    $('#cboxClose').remove();
		}        
	    }); 
	});
      
                
	if({$brand} != '0') {
	    var brandParam = '/brand='+urlencode({$brand});
	} else {
	    var brandParam = '';
	} 

	if({$serviceProvider} != '0') {
	    var serviceProviderParam = '/serviceProvider=' + urlencode({$serviceProvider});
	} else {
	    var serviceProviderParam = '';
	} 
                
	if({$sType} != '0') {
	    var sTypeParam = '/sType=' + urlencode({$sType});
	} else {
	    var sTypeParam = '';
	}

	if({$scType} != '0') {
	    var scTypeParam = '/scType=' + urlencode({$scType});
	} else {
	    var scTypeParam = '';
	}

	if({$manufacturer} != '0') {
	    var manufacturerParam = '/manufacturer=' + urlencode({$manufacturer});
	} else {
	    var manufacturerParam = '';
	}
        
        if({$network} != '0') {
	    var networkParam = '/network=' + urlencode({$network});
	} else {
	    var networkParam = '';
	}
        
        if({$branch} != '0') {
	    var branchParam = '/branch=' + urlencode({$branch});
	} else {
	    var branchParam = '';
	}
        

	if({$client} != '0') {
	    var clientParam = '/client=' + urlencode({$client});
	} else {
	    var clientParam = '';
	}
        
        if({$unitType} != '0') {
	    var unitTypeParam = '/unitType=' + urlencode({$unitType});
	} else {
	    var unitTypeParam = '';
	}
        
        if({$skillSet} != '0') {
	    var skillSetParam = '/skillSet=' + urlencode({$skillSet});
	} else {
	    var skillSetParam = '';
	}
        
        
        if('{$ojBy}' != '') {
	    var ojByParam = '/ojBy='+urlencode('{$ojBy}');
	} else {
	    var ojByParam = '';
	}
        
        if('{$ojvBy}' != '') {
	    var ojvByParam = '/ojvBy='+urlencode('{$ojvBy}');
	} else {
	    var ojvByParam = '';
	}
        
        
        if('{$btnName}' != '') {
	    var btnNameParam = '/btnName='+urlencode('{$btnName}');
	} else {
	    var btnNameParam = '';
	}
        
        
        if('{$btnValue}' != '') {
	    var btnValueParam = '/btnValue='+urlencode('{$btnValue}');
	} else {
	    var btnValueParam = '';
	}
        
        if('{$btnValue2}' != '') {
	    var btnValue2Param = '/btnValue2='+urlencode('{$btnValue2}');
	} else {
	    var btnValue2Param = '';
	}
        
        
	/*
	//Open job result data table starts here...  
	$('#openJobsResults').PCCSDataTable({
	    aoColumns: [
		{ sWidth: "7%" },
		{ sWidth: "10%", sType: 'date-eu'},
		{ sWidth: "6%" },
		{ sWidth: "8%" },
		{ sWidth: "14%" },
		{ sWidth: "7%" },
		{ sWidth: "14%" },
		{ sWidth: "12%" },
		{ sWidth: "13%" },
		{ sWidth: "5%" },
		{ sWidth: "4%", bSortable: false }
	    ],
	    displayButtons:	"P",
	    bServerSide:	true,
	    htmlTablePageId:    'openJobsResultsPanel',
	    htmlTableId:        'openJobsResults',
	    fetchDataUrl:       '{$_subdomain}/Job/openJobsResult' + brandParam + sTypeParam + scTypeParam + manufacturerParam + clientParam + serviceProviderParam + ojByParam + btnNameParam + btnValueParam + btnValue2Param ,
	    pickCallbackMethod: 'openJob',
	    dblclickCallbackMethod: 'openJob',
	    tooltipTitle:	"{$page['Text']['tooltip_title']|escape:'html'}",
	    searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
	    colorboxForceClose:	false
	});
	//Open job result data table ends here...  
	*/
	
	
	table = $("#openJobsResults").dataTable({
            bAutoWidth:	false,
            
            
	   
            aoColumns: [ 
			
			
			{for $er=0 to $data_keys|@count}                    
                            {$vis='true'}  
                              {if isset($data_keys.$er) && $data_keys.$er}
                                { 'mDataProp':"{$data_keys.$er}",'bVisible': {$vis} ,'bSearchable': true, 'sName': "{$data_keys.$er}",'sDefaultContent':"" },
                              {/if}
                                
                        {/for} 
                      { 'mDataProp':'0','bSortable': false }
                            
                            
		],
              
            bDestroy:           true,
            bStateSave:         true,
            fnStateSave: function(oSettings, oData) { save_dt_view(oSettings, oData); },
            fnStateLoad: function(oSettings) { return load_dt_view(oSettings); },
            bServerSide:        true,
            bProcessing:        false,
            htmlTableId:        "openJobsResults",
            sDom:               "Rft<'#dataTables_child'>Trpli",
            "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
                       
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
                                
                                $('.text_filter').each(function(){
                                width=$(this).closest('th').width()-8;
                                $(this).width(width);
      
        //console.log(width);
    });
				fnCallback(json)
                                
                               
			} );
                        },
            sPaginationType:    "full_numbers",
            "fnInitComplete": function(oSettings, json) {
             alert( 'DataTables has finished its initialisation.' );
            },
            bPaginate:          true,
            bSearch:            true,
            "aLengthMenu": [[ 10,25, 50, 100 ,500,1000, -1], [10,25, 50, 100,500,1000, "All"]],
            iDisplayLength:     50,
            "aaSorting": [ [4,'desc'] ],
            sAjaxSource:        '{$_subdomain}/Job/openJobsResult' + brandParam + sTypeParam + scTypeParam + manufacturerParam + networkParam + branchParam + clientParam + unitTypeParam + skillSetParam + serviceProviderParam + ojByParam + ojvByParam + btnNameParam + btnValueParam + btnValue2Param+'/',
            oTableTools: {
            "sSwfPath": "{$_subdomain}/swf/copy_csv_xls_pdf.swf",
                sRowSelect: "single",
                
                aButtons: [
                    {
                        sExtends:	"text",
                        sButtonText:    "View Job Details",
                        fnClick: function() { 
			    var tbl = TableTools.fnGetInstance("openJobsResults");
			    var data = tbl.fnGetSelectedData();
			    location.assign("{$_subdomain}/index/jobupdate/" + data[0][0] + "/?ref=openJobs");
			}
                    },"copy","print","csv","xls"
                    {if $AP9000} {* Check if permission for Servicebase refresh *}
                    ,
                    {
                        sExtends:	"text",
                        sButtonText:    "ServiceBase Refresh",
                        fnClick: function() { 
                            var selectedJobIds = '';
                            $("input:checked", table.fnGetNodes()).each(function(){
                                // For each selected job get the JobID (from teh first (hidden) field in the datatable and add it to the string
                                selectedJobIds = selectedJobIds + $('tr:eq('+(table.fnGetPosition($(this).closest('tr')[0])+2)+') td', table).html() + ',' ;
                            });
                            if (selectedJobIds != '') {
                                selectedJobIds = selectedJobIds.substring(0, selectedJobIds.length - 1);     // Remove trailing comma
                                //location.assign("{$_subdomain}/Job/serviceBaseRefresh/" + selectedJobIds);
                                $.colorbox({ 
                                        href:"{$_subdomain}/Job/serviceBaseRefresh/" + selectedJobIds+"/ojl",
                                        title: "Servicebase Referesh",
                                        opacity: 0.75,
                                        overlayClose: false,
                                        escKey: false,
                                        width:		800
                                });
                            } /* fi selectedJobIds != '' */
			} /* fnClick */
                    }
                    {/if} {* $AP9000 *}
                    {if $AP9001} {* Check if permission for Servicebase refresh *}
                    ,
                    {
                        sExtends:	"text",
                        sButtonText:    "Bulk Refresh",
                        fnClick: function() { 
                            location.assign("{$_subdomain}/Job/bulkSbRefresh/");
                        } /* fnClick */
                    } {/if} {* $AP9001 *}
                ],
                
                fnRowSelected: function(node) { /*selectStatus(node);*/ }
            },
	    oLanguage: {
		sSearch: "<span id='searchLabel' style='float:left; top:10px; position:relative;'>Search within results:</span>&nbsp;",
                "sProcessing": "<img src='{$_subdomain}/images/ajax-loading_medium.gif'>"
	    },
	    fnInitComplete: function() {
		var html = '<a href="#" id="delSearch" style="top:14px; right:4px; position:absolute;">\
				<img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
			    </a>';
		$(html).insertAfter(".dataTables_filter input");
		$(document).on("click", "#delSearch", function() {
		    $(".dataTables_filter input").val("");
		    table.fnFilter("");
		    return false;
		});
	    },
	    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            
                if (aData['ID'] == {$selectedJobId}) {
                    $(nRow).addClass('row_selected');
                }
            
            
		if(parseInt(aData[3]) > 0) {
		    var cell = $(nRow).find("td").toArray()[3];
		    $(cell).html("<span style='color:red; font-weight:bold'>" + $(cell).html() + "</span>");
		}
	    }
            
            
            
	}).columnFilter({
         aoColumns: [ 
			
			
			{for $er=0 to $data_keys|@count-1}                    
                            {$vis='true'}  
                              
                               
                                { 'bVisible': true ,'bSearchable': true },
                        {/for} 
                         null
                            
                            
		]
        }).fnSetFilteringDelay(500);
	table.fnSetColumnVis( 0,false);
        
        
        function fnResetAllFilters() {
        
    var oSettings = table.fnSettings();
    for(iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
        oSettings.aoPreSearchCols[ iCol ].sSearch = '';
    }
     $('.text_filter').each(function(){
                               
                                $(this).val('').focus().blur();
                                
      
     
    });
    table.fnDraw();
    //console.log(oSettings);
}
        
        $('#check_all').click( function() {
            $('input', table.fnGetNodes()).attr('checked',this.checked);
            countTagged();
        } );
        $('#clearAllFilters').click( function() {
          fnResetAllFilters();
        } );
		

	$(document).on("dblclick", "tr", function() { 

            if(table.fnGetData(this) !== null){
                location.assign("{$_subdomain}/index/jobupdate/" + table.fnGetData(this)['ID'] + "/?ref=openJobs");
            }
	});
        
        $(document).on("click", "tr", function() { 
            if(table.fnGetData(this) !== null){ 
                table.find('tr.row_selected').removeClass('row_selected');
                $.ajax({
                        url:'{$_subdomain}/Job/setchart/',
                        data:'id='+table.fnGetData(this)['ID']+"&ref=openJobs",
                        type:'post',
                        dataType:'html',
                        success:function(){
                        }
                });
            }
	});


	//Slider code starts here..
               
	//scrollpane parts
	var scrollPane = $(".scroll-pane"), scrollContent = $(".scroll-content");

	//build slider
	var scrollbar = $(".scroll-bar").slider({
	    slide: function(event, ui) {
		if(scrollContent.width() > scrollPane.width()) {
		    scrollContent.css("margin-left", Math.round(ui.value / 100 * (scrollPane.width() - scrollContent.width())) + "px");
		} else {
		    scrollContent.css("margin-left", 0);
		}
	    }
	});

	//append icon to handle
	var handleHelper = scrollbar.find(".ui-slider-handle").mousedown(function() {
	    scrollbar.width(handleHelper.width());
	}).mouseup(function() {
	    scrollbar.width( "100%" );
	}).append("<span class='ui-icon ui-icon-grip-dotted-vertical'></span>").wrap("<div class='ui-handle-helper-parent'></div>").parent();

	//change overflow to hidden now that slider handles the scrolling
	scrollPane.css("overflow", "hidden");

	//size scrollbar and handle proportionally to scroll distance
	function sizeScrollbar() {
	    var remainder = scrollContent.width() - scrollPane.width();
	    var proportion = remainder / scrollContent.width();
	    var handleSize = scrollPane.width() - (proportion * scrollPane.width());
	    scrollbar.find(".ui-slider-handle").css({
		width: handleSize,
		"margin-left": -handleSize / 2
	    });
	    handleHelper.width("").width(scrollbar.width() - handleSize);
	}

	//reset slider value based on scroll content position
	function resetValue() {
	    var remainder = scrollPane.width() - scrollContent.width();
	    var leftVal = scrollContent.css("margin-left") === "auto" ? 0 : parseInt(scrollContent.css("margin-left"));
	    var percentage = Math.round( leftVal / remainder * 100 );
	    scrollbar.slider( "value", percentage );
	}

	//if the slider is 100% and window gets larger, reveal content
	function reflowContent() {
	    var showing = scrollContent.width() + parseInt(scrollContent.css("margin-left"), 10);
	    var gap = scrollPane.width() - showing;
	    if(gap > 0) {
		scrollContent.css("margin-left", parseInt(scrollContent.css("margin-left"), 10) + gap);
	    }
	}

	//change handle position on window resize
	$(window).resize(function() {
	    resetValue();
	    sizeScrollbar();
	    reflowContent();
	});
	
	//init scrollbar size
         sizeScrollbar(); //other browsers dont want timeout :)
	//setTimeout(sizeScrollbar, 10); //safari wants a timeout
	    {if $SelectedBrands|@count lt 9 && $SelectedManufacturers|@count lt 9 && $SelectedServiceProviders|@count lt 9 && $SelectedNetworks|@count lt 9 && $SelectedBranch|@count lt 9 && $SelectedClient|@count lt 9 && $SelectedUnitType|@count lt 9 && $SelectedSkillSet|@count lt 9}
		$("#brand-scroll-bar").hide();
		$("#manufacturer-scroll-bar").hide();
		$("#service-provider-scroll-bar").hide();
		$("#network-scroll-bar").hide();
		$("#branch-scroll-bar").hide();
		$("#client-scroll-bar").hide();
		$("#unit-type-scroll-bar").hide();
		$("#skill-set-scroll-bar").hide();
	    {/if}

	//Slider code ends here..
               
               
	$("#tabs").tabs({
	    selected: parseInt($("#PreferredClientGroupTabID").val() - 1), 
	    select: function(event, ui) { 
		$value = ui.panel.id.replace("tabs-", "");
		if($value) {
		    $("#PreferredClientGroupTabID").val($value);
		    $(".TimeSlotActiveTab").removeClass("TimeSlotActiveTab");
		    $("#Tab"+$("#PreferredClientGroupTabID").val() + " a").addClass("TimeSlotActiveTab");
		} else {
		    $("#PreferredClientGroupTabID").val(0);
		    $(".TimeSlotActiveTab").removeClass("TimeSlotActiveTab");
		    $("#Tab"+$("#PreferredClientGroupTabID").val() + " a").addClass("TimeSlotActiveTab");
		}
		scrollContent.css( "margin-left", 0 );
	    }
	});
        
      $("#Tab"+$("#PreferredClientGroupTabID").val()+" a").trigger('click');
       
    });
     
     
    //Open jobs chart starts here..
    google.load("visualization", "1", { packages:["corechart"] });
    google.setOnLoadCallback(drawChart);
    
    function drawChart() {
	var data = new google.visualization.DataTable();
	var gridLines = 0;
               
	var $statsResult = [
	    {foreach $statsResult as $sname}  
	       ["{$sname[0]}", {$sname[1]}, {$sname[2]}, "{$sname[3]}"]{if not $sname@last},{/if} 
	    {/foreach}
	];

	data.addColumn('string', "{$page['Text']['status_types']|escape:'html'}");
	$total_rows = 0;
	$data_rows  = new Array();
	$data_rows[0] = new Array();
	$data_rows[0][0] = '';
	maxrows = 0;
	$colorsData = new Array();
                
	$cCnt = 1;
                
	for(var $i = 0; $i < $statsResult.length; $i++) {
	    data.addColumn('number', $statsResult[$i][0], $statsResult[$i][2]);
	    data.addColumn({ type: 'string', role: 'tooltip' }); // tooltip col.
	    $total_rows += $statsResult[$i][1];       
                        
	    //$data_rows[0][$i+1] = $statsResult[$i][1];
	    $data_rows[0][$cCnt++] = $statsResult[$i][1];
	    $data_rows[0][$cCnt++] = $statsResult[$i][0];

	    if(gridLines <  $statsResult[$i][1]) {
		gridLines =  $statsResult[$i][1] + 1;
		maxrows = $statsResult[$i][1];
	    }

	    $colorsData[$i] = "#" + $statsResult[$i][3];
	}
                
	data.addRows($data_rows);

	var options = {
	    'chartArea': { 
		'width': $(window).width()-350, 
		'height': '80%',
		'left' : '70' 
	    },
	    'legend': { 
		'position' : 'right',
		'textStyle' : { 'fontSize' : '9' } 
	    },
	    'vAxis': { 
		title: "{$page['Text']['quantity']|escape:'html'}", 
		titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
		slantedText: false,
		minValue: 0,
		maxValue: maxrows,
		textColor: '#ffffff',
		gridlines: { count: gridLines }
	    },
	    'hAxis': { 
		title: "{$graph_title}",
		titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
		slantedText: false
	    },
	    'colors': $colorsData      
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
	chart.draw(data, options);
                
	//a click handler which grabs some values then redirects the page
	google.visualization.events.addListener(chart, 'select', function() {
	    //grab a few details before redirecting
                var selection = chart.getSelection();
                
	    //var col = selection[0].column;
	    //alert(chart.getColumnId(selection[0].column));
                
	    var col = data.getColumnId(selection[0].column);
	    //alert('The user selected ' + topping);
                
	    document.location.href = '{$_subdomain}/Job/openJobs' + '/client=' + urlencode({$client}) + '/manufacturer=' + urlencode({$manufacturer}) + '/network=' + urlencode({$network}) + '/brand=' + urlencode({$brand}) + '/branch=' + urlencode({$branch}) + '/client=' + urlencode({$client}) + '/unitType=' + urlencode({$unitType}) + '/skillSet=' + urlencode({$skillSet}) + '/ojBy=' + urlencode('{$ojBy}') + '/btnName=' + urlencode('{$btnName}') + '/btnValue=' + urlencode('{$btnValue}') + '/btnValue2=' + urlencode('{$btnValue2}')   + '/serviceProvider=' + urlencode({$serviceProvider}) + '/sType=' + col;
               
	    });
	}
      
	//Open jobs chart ends here..
      
      
      //displaying table pref colorbox
function showTablePreferences(){
$.colorbox({ 
 
                        href:"{$_subdomain}/Job/tableDisplayPreferenceSetup/page=openJobs/table=job",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
      function countTagged(){
      ch=0;
      $('.taggedRec').each(function(){
      if(this.checked) {
      ch+=1
      }
      });
      if(ch>0){
     
      $('#recordsTaggedDiv').show();
      $('#recordsTaggedSpan').html(ch);
      
      }else{
       $('#recordsTaggedDiv').hide();
      }
      }
</script>


{/block}


{block name=body}

<style>
    td{
        text-align:left !important;
      }

</style>
<div style="float:right">
    <a href="#" onclick="showTablePreferences();">Display Preferences</a>
</div>
<div class="breadcrumb" {if $fullscreen==true}style="width:100%"{/if} >
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
    </div>
</div>
{include file='include/menu.tpl'}
<div class="main" id="home" {if $fullscreen==true}style="width:100%;min-width: 950px"{/if}>
    <div class="openJobsPanel" {if $fullscreen==true}style="min-width: 950px;margin: auto"{/if}>
        <form id="openJobsForm" name="openJobsForm" method="post"  action="#" class="inline">
             
	    <fieldset>
		<legend title="">{$page['Text']['openjob_legend']|escape:'html'}</legend>
		<p style="text-align:right;">
                    
                    <span style="margin-right:50px;" >
                        {$page['Labels']['openjobs_by']|escape:'html'}: 
                        <input type="radio" name="ojBy" class="ojByElement" value="sp" {if $ojBy neq 'b'} checked="checked" {/if} > {$page['Labels']['service_provider']|escape:'html'}
                        <input type="radio" name="ojBy" class="ojByElement"  value="b" {if $ojBy eq 'b'}  checked="checked" {/if} > {$page['Labels']['branch']|escape:'html'}
                    </span>
                    
                    
                    
                    
                    {*
                    <span style="margin-right:50px;" >
                        {$page['Labels']['tat_view']|escape:'html'}: 
                        <input type="radio" name="tView" class="tViewElement" value="s" {if $tView neq 'd'} checked="checked" {/if} > {$page['Labels']['summary']|escape:'html'}
                        <input type="radio" name="tView" class="tViewElement"  value="d" {if $tView eq 'd'} checked="checked" {/if} > {$page['Labels']['detail']|escape:'html'}
                    </span>
                    *}
		</p>    
		<p>
                    <div class="innerFieldSet">
                        <fieldset>
                            <legend title="" class="innerLegend">{$page['Text']['preferred_client_group']|escape:'html'}</legend>
                            <p style="text-align: right">
                                {if $SelectedBrands|@count gt 0 || $SelectedManufacturers|@count gt 0 || $SelectedServiceProviders|@count gt 0 || $SelectedNetworks|@count gt 0 || $SelectedBranch|@count gt 0 || $SelectedClient|@count gt 0 || $SelectedUnitType|@count gt 0 || $SelectedSkillSet|@count gt 0}
                                    <a href="{$_subdomain}/Job/openJobs" class="showalljobs" style="font-size:12px;">{$page['Text']['show_all']|escape:'html'}</a>
                                    &nbsp;&nbsp;
                                {/if}
                                <a href="#" id="PreferentialClientsManufacturers" style="font-size:12px;" >{$page['Text']['setup']|escape:'html'}</a>&nbsp;
                            </p>
                            {if $SelectedBrands|@count gt 0 || $SelectedManufacturers|@count gt 0 || $SelectedServiceProviders|@count gt 0 || $SelectedClient|@count gt 0 || $SelectedNetworks|@count gt 0 || $SelectedBranch|@count gt 0 || $SelectedClient|@count gt 0 || $SelectedUnitType|@count gt 0 || $SelectedSkillSet|@count gt 0}
				<div id="tabs" style="border-style:none;height:80px;">
				    <ul style="background-color:transparent;background-image: url('');border-style:none;">
					{if $BrandGroupEnable eq 1}         {* check Grpup Enable for Brand to show tab*}
                                        <li id="Tab1">
					    <a href="#tabs-1" style="font-size:12px;">
						{$page['Text']['preferential_brands']|escape:'html'}
					    </a>
					</li>
                                        {/if}
                                        {if $ManufacturerGroupEnable eq 1}      {* check Grpup Enable for Manufacturer*}
					<li id="Tab2">
					    <a href="#tabs-2" style="font-size:12px;">
						{$page['Text']['preferential_manufacturers']|escape:'html'}
					    </a>
					</li>
                                        {/if}
                                        {if $ServiceProviderGroupEnable eq 1}   {* check Grpup Enable for Service Provider*}
					<li id="Tab3">
					    <a href="#tabs-3" style="font-size:12px;">
						{$page['Text']['preferential_service_providers']|escape:'html'}
					    </a>
					</li>
                                        {/if}
                                        {* Added New Tabs for Networks,Branches,Clients, Skillsets and Unit Types*}
                                        {if $NetworkGroupEnable eq 1}
					<li id="Tab4">
					    <a href="#tabs-4" style="font-size:12px;">
						{$page['Text']['preferential_networks']|escape:'html'}
					    </a>
					</li>
                                        {/if}
                                        {if $BranchGroupEnable eq 1}
					<li id="Tab5">
					    <a href="#tabs-5" style="font-size:12px;">
						{$page['Text']['preferential_branches']|escape:'html'}
					    </a>
					</li>
                                        <li id="Tab5">
					    <a href="#tabs-5" style="font-size:12px;">
						{$page['Text']['preferential_branches']|escape:'html'}
					    </a>
					</li>
                                        {/if}
                                        {if $ClientGroupEnable eq 1}
					<li id="Tab6">
					    <a href="#tabs-6" style="font-size:12px;">
						{$page['Text']['preferential_clients']|escape:'html'}
					    </a>
					</li>
                                        {/if}
                                        {if $UnitTypeGroupEnable eq 1}
					<li id="Tab7">
					    <a href="#tabs-7" style="font-size:12px;">
						{$page['Text']['preferential_unit_types']|escape:'html'}
					    </a>
					</li>
                                        {/if}
                                        {if $SkillSetGroupEnable eq 1}
					<li id="Tab8">
					    <a href="#tabs-8" style="font-size:12px;">
						{$page['Text']['preferential_skill_set']|escape:'html'}
					    </a>
					</li>
                                        {/if}
				    </ul>
                                    {if $BrandGroupEnable eq 1}                         {* check Grpup Enable for Brand to show contenst of tab *}
				    <div id="tabs-1" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedBrands|@count gt 0}
						    {foreach from=$SelectedBrands item=sb}
							<span class="leftAlign {if $brand eq $sb.BrandID} SelectedPreferredLogo {/if}">
							    <a href="{$_subdomain}/Job/openJobs/brand={$sb.BrandID}{if $manufacturer neq 0}/manufacturer={$manufacturer}{/if}{if $network neq 0}/network={$network}{/if}{if $serviceProvider neq 0}/serviceProvider={$serviceProvider}{/if}{if $branch neq 0}/branch={$branch}{/if}{if $client neq 0}/client={$client}{/if}{if $unitType neq 0}/unitType={$unitType}{/if}{if $skillSet neq 0}/skillSet={$skillSet}{/if}" title="Click here to get all {$sb.BrandName|escape:'html'} Open Jobs" >
								{if $sb.BrandLogo}
                                                                    
                                                                    <img src="{$_subdomain}/images/brandLogos/{$sb.BrandLogo|escape:'html'}" class="PreferredLogo">
                                                                {else if $sb.Acronym}
                                                                    {$sb.Acronym|escape:'html'}
                                                                {else}
                                                                    {$sb.BrandName|substr:0:8|escape:'html'}
                                                                     {if $sb.BrandName|strlen gt 8}
                                                                         &hellip;
                                                                     {/if}
                                                                {/if}
                                                            </a>
							</span>
						    {/foreach}  
						{else}
						    {$page['Text']['no_results']|escape:'html'}
						{/if}
					    </div>
					    <div id="brand-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>
                                    {/if}
                                    {if $ManufacturerGroupEnable eq 1}          {* check Grpup Enable for Manufacturer to show contenst of tab *}
				    <div id="tabs-2" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedManufacturers|@count gt 0}
						    {foreach from=$SelectedManufacturers item=sm}
							 <span class="leftAlign {if $manufacturer eq $sm.ManufacturerID} SelectedPreferredLogo {/if}" >
							     <a href="{$_subdomain}/Job/openJobs/manufacturer={$sm.ManufacturerID}{if $brand neq 0}/brand={$brand}{/if}{if $network neq 0}/network={$network}{/if}{if $serviceProvider neq 0}/serviceProvider={$serviceProvider}{/if}{if $branch neq 0}/branch={$branch}{/if}{if $client neq 0}/client={$client}{/if}{if $unitType neq 0}/unitType={$unitType}{/if}{if $skillSet neq 0}/skillSet={$skillSet}{/if}" title="Click here to get all {$sm.ManufacturerName|escape:'html'} Open Jobs">
								 {if $sm.ManufacturerLogo}
                                                                     
                                                                    <img src="{$_subdomain}/images/manufacturerLogos/{$sm.ManufacturerLogo|escape:'html'}" class="PreferredLogo">
                                                                 {else if $sm.Acronym}
                                                                    {$sm.Acronym|escape:'html'}
                                                                 {else}
                                                                     {$sm.ManufacturerName|substr:0:8|escape:'html'}
                                                                     {if $sm.ManufacturerName|strlen gt 8}
                                                                         &hellip;
                                                                     {/if}    
                                                                 {/if}    
                                                             </a>
							 </span>
						    {/foreach}
						 {else}   
						    {$page['Text']['no_results']|escape:'html'}
						 {/if}   
					    </div>
					    <div id="manufacturer-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>
                                    {/if}
                                    {if $ServiceProviderGroupEnable eq 1}                               {* check Grpup Enable for Service Provider to show contenst of tab *}
				    <div id="tabs-3" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedServiceProviders|@count gt 0}
						    {foreach from=$SelectedServiceProviders item=ssp}
							<span {if $ssp.Acronym|strlen <= 12}style="padding-top:7px;"{else}style="padding-top:0px;height:34px"{/if} class="leftAlign {if $serviceProvider eq $ssp.ServiceProviderID} SelectedPreferredLogo {/if}">
							    <a href="{$_subdomain}/Job/openJobs/serviceProvider={$ssp.ServiceProviderID}{if $brand neq 0}/brand={$brand}{/if}{if $network neq 0}/network={$network}{/if}{if $manufacturer neq 0}/manufacturer={$manufacturer}{/if}{if $branch neq 0}/branch={$branch}{/if}{if $client neq 0}/client={$client}{/if}{if $unitType neq 0}/unitType={$unitType}{/if}{if $skillSet neq 0}/skillSet={$skillSet}{/if}/sType=0" title="Click here to get all {$ssp.CompanyName|escape:'html'} Open Jobs" >
								{*<img src="{$_subdomain}/images/brandLogos/{$ssp.ServiceProviderLogo|escape:'html'}" class="PreferredLogo">*}
                                                                {if $ssp.Acronym}
                                                                    {$ssp.Acronym|escape:'html'}
                                                                {else}
                                                                    {$ssp.CompanyName|substr:0:8|escape:'html'}
                                                                    {if $ssp.CompanyName|strlen gt 8}
                                                                        &hellip;
                                                                    {/if}  
                                                                {/if}   
							    </a>
							</span>
						    {/foreach}  
						{else}
						    {$page['Text']['no_results']|escape:'html'}
						{/if}
					    </div>
					    <div id="service-provider-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>         
                                    {/if}
                                    
                                    {* Added Contents for Network,Branch,Client,Unit Type and Skillset, on groupEnable checked*}
                                    
                                    {if $NetworkGroupEnable eq 1}
				    <div id="tabs-4" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedNetworks|@count gt 0}
						    {foreach from=$SelectedNetworks item=ssp}
							<span class="leftAlign {if $network eq $ssp.NetworkID} SelectedPreferredLogo {/if}">
							    <a href="{$_subdomain}/Job/openJobs/network={$ssp.NetworkID}{if $brand neq 0}/brand={$brand}{/if}{if $serviceProvider neq 0}/serviceProvider={$serviceProvider}{/if}{if $manufacturer neq 0}/manufacturer={$manufacturer}{/if}{if $branch neq 0}/branch={$branch}{/if}{if $client neq 0}/client={$client}{/if}{if $unitType neq 0}/unitType={$unitType}{/if}{if $skillSet neq 0}/skillSet={$skillSet}{/if}" title="Click here to get all {$ssp.CompanyName|escape:'html'} Open Jobs" >
                                                                {$ssp.CompanyName|substr:0:8|escape:'html'}
                                                                {if $ssp.CompanyName|strlen gt 8}
                                                                    &hellip;
                                                                {/if}  
							    </a>
							</span>
						    {/foreach}  
						{else}
						    {$page['Text']['no_results']|escape:'html'}
						{/if}
					    </div>
					    <div id="network-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>         
                                    {/if}
                                    {if $BranchGroupEnable eq 1}
				    <div id="tabs-5" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedBranch|@count gt 0}
						    {foreach from=$SelectedBranch item=ssp}
							<span class="leftAlign {if $branch eq $ssp.BranchID} SelectedPreferredLogo {/if}">
							    <a href="{$_subdomain}/Job/openJobs/branch={$ssp.BranchID}{if $brand neq 0}/brand={$brand}{/if}{if $manufacturer neq 0}/manufacturer={$manufacturer}{/if}{if $serviceProvider neq 0}/serviceProvider={$serviceProvider}{/if}{if $network neq 0}/network={$network}{/if}{if $client neq 0}/client={$client}{/if}{if $unitType neq 0}/unitType={$unitType}{/if}{if $skillSet neq 0}/skillSet={$skillSet}{/if}" title="Click here to get all {$ssp.BranchName|escape:'html'} Open Jobs" >
                                                                {$ssp.BranchName|substr:0:8|escape:'html'}
                                                                {if $ssp.BranchName|strlen gt 8}
                                                                    &hellip;
                                                                {/if}  
							    </a>
							</span>
						    {/foreach}  
						{else}
						    {$page['Text']['no_results']|escape:'html'}
						{/if}
					    </div>
					    <div id="branch-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>         
                                    {/if}
                                    {if $ClientGroupEnable eq 1}
				    <div id="tabs-6" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedClient|@count gt 0}
						    {foreach from=$SelectedClient item=ssp}
							<span class="leftAlign {if $client eq $ssp.ID} SelectedPreferredLogo {/if}">
							    <a href="{$_subdomain}/Job/openJobs/client={$ssp.ID}{if $brand neq 0}/brand={$brand}{/if}{if $manufacturer neq 0}/manufacturer={$manufacturer}{/if}{if $serviceProvider neq 0}/serviceProvider={$serviceProvider}{/if}{if $network neq 0}/network={$network}{/if}{if $branch neq 0}/branch={$branch}{/if}{if $unitType neq 0}/unitType={$unitType}{/if}{if $skillSet neq 0}/skillSet={$skillSet}{/if}" title="Click here to get all {$ssp.CompanyName|escape:'html'} Open Jobs" >
                                                                {$ssp.CompanyName|substr:0:8|escape:'html'}
                                                                {if $ssp.CompanyName|strlen gt 8}
                                                                    &hellip;
                                                                {/if}  
							    </a>
							</span>
						    {/foreach}  
						{else}
						    {$page['Text']['no_results']|escape:'html'}
						{/if}
					    </div>
					    <div id="client-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>         
                                    {/if}
                                    {if $UnitTypeGroupEnable eq 1}
				    <div id="tabs-7" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedUnitType|@count gt 0}
						    {foreach from=$SelectedUnitType item=ssp}
							<span class="leftAlign {if $unitType eq $ssp.UnitTypeID} SelectedPreferredLogo {/if}">
							    <a href="{$_subdomain}/Job/openJobs/unitType={$ssp.UnitTypeID}{if $brand neq 0}/brand={$brand}{/if}{if $manufacturer neq 0}/manufacturer={$manufacturer}{/if}{if $serviceProvider neq 0}/serviceProvider={$serviceProvider}{/if}{if $network neq 0}/network={$network}{/if}{if $branch neq 0}/branch={$branch}{/if}{if $skillSet neq 0}/skillSet={$skillSet}{/if}" title="Click here to get all {$ssp.UnitTypeName|escape:'html'} Open Jobs" >
                                                                {$ssp.UnitTypeName|substr:0:8|escape:'html'}
                                                                {if $ssp.UnitTypeName|strlen gt 8}
                                                                    &hellip;
                                                                {/if}  
							    </a>
							</span>
						    {/foreach}  
						{else}
						    {$page['Text']['no_results']|escape:'html'}
						{/if}
					    </div>
					    <div id="unit-type-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>         
                                    {/if}
                                    {if $SkillSetGroupEnable eq 1}
				    <div id="tabs-8" style="padding-left:3px;padding-right:0px;">
					<div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
					    <div class="scroll-content">
						{if $SelectedSkillSet|@count gt 0}
						    {foreach from=$SelectedSkillSet item=ssp}
							<span class="leftAlign {if $skillSet eq $ssp.RepairSkillID} SelectedPreferredLogo {/if}">
							    <a href="{$_subdomain}/Job/openJobs/skillSet={$ssp.RepairSkillID}{if $brand neq 0}/brand={$brand}{/if}{if $manufacturer neq 0}/manufacturer={$manufacturer}{/if}{if $serviceProvider neq 0}/serviceProvider={$serviceProvider}{/if}{if $network neq 0}/network={$network}{/if}{if $branch neq 0}/branch={$branch}{/if}{if $unitType neq 0}/unitType={$unitType}{/if}" title="Click here to get all {$ssp.RepairSkillName|escape:'html'} Open Jobs" >
                                                                {$ssp.RepairSkillName|substr:0:8|escape:'html'}
                                                                {if $ssp.RepairSkillName|strlen gt 8}
                                                                    &hellip;
                                                                {/if}  
							    </a>
							</span>
						    {/foreach}  
						{else}
						    {$page['Text']['no_results']|escape:'html'}
						{/if}
					    </div>
					    <div id="skill-set-scroll-bar" class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
						<div class="scroll-bar"></div>
					    </div>
					</div>
				    </div>         
                                    {/if}
				</div>

				<input type="hidden" name="PreferredClientGroupTabID" id="PreferredClientGroupTabID" value="{if $manufacturer neq 0}2{elseif $serviceProvider neq 0}3{elseif $network neq 0}4{elseif $branch neq 0}5{elseif $client neq 0}6{elseif $unitType neq 0}7{elseif $skillSet neq 0}8{else}1{/if}">
                            {/if}
			    </fieldset> 

			</div>

		    

		</p>
            
		<p>
		    <div id="chart_div"></div>
                    <a href="#" id="StatusPreferences" style="float:right;font-size:12px;margin-left: 25px;">{$page['Text']['status_preferences']|escape:'html'}</a>&nbsp;&nbsp;
                    <a href="#" id="RefreshStatusPreferences" style="float:right;font-size:12px; " >{$page['Text']['refresh_status']|escape:'html'}</a>&nbsp;&nbsp;
		</p>
          
	    </fieldset> 
        
	</form>
			
    </div>   
                                    
       
    <div class="openJobsResultsPanel">
        
        <table id="CallManagerOptions" class="PlainTable" style="width:1024px;"  align="left" >
            <tr>
                <td></td>
                {if $TatResult['TatID'] neq ''}
                    <td class="topTd" style="text-align:center!important;"> < {$TatResult['Button1']|escape:'html'} {$page['Text']['days_text']|escape:'html'}</td>
                    <td class="topTd" style="text-align:center!important;">{$TatResult['Button1']|escape:'html'} {$page['Text']['to_text']|escape:'html'} {$TatResult['Button3']|escape:'html'} {$page['Text']['days_text']|escape:'html'}</td>
                    <td class="topTd" style="text-align:center!important;"> > {$TatResult['Button3']|escape:'html'} {$page['Text']['days_text']|escape:'html'}</td>
                    <td class="topTd" style="text-align:center!important;"> > {$TatResult['Button4']|escape:'html'} {$page['Text']['days_text']|escape:'html'}</td>
                {/if}
                <td></td>
                <td></td>
            </tr>
            
            <tr class="odd" >
                <td style="width: 150px;">
                {$page['Labels']['open_job_tat_alert']|escape:'html'}: 
                </td>
            
                {if $TatResult['TatID'] neq ''}

                  <td> <input {if $btnName=='b1'} class="ActiveButton TatButton" {else} class="TatButton" {/if} style="min-width:100px;background-color:#{$TatResult['Button1Colour']|escape:'html'};"   type="submit" name="tat_button1" value="{$TatResult['Button1Value']|escape:'html'}" > </td>
                  <td> <input {if $btnName=='b2'} class="ActiveButton TatButton" {else} class="TatButton" {/if} style="min-width:100px;background-color:#{$TatResult['Button2Colour']|escape:'html'};"   type="submit" name="tat_button2" value="{$TatResult['Button2Value']|escape:'html'}" > </td>
                  <td> <input {if $btnName=='b3'} class="ActiveButton TatButton" {else} class="TatButton" {/if} style="min-width:100px;background-color:#{$TatResult['Button3Colour']|escape:'html'};"   type="submit" name="tat_button3" value="{$TatResult['Button3Value']|escape:'html'}" > </td>
                  <td> <input {if $btnName=='b4'} class="ActiveButton TatButton" {else} class="TatButton" {/if} style="min-width:100px;background-color:#{$TatResult['Button4Colour']|escape:'html'};"   type="submit" name="tat_button4" value="{$TatResult['Button4Value']|escape:'html'}" > </td>

                {/if}
            
                <td style="width:60px;"> 
                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="OpenTatHelp" class="helpTextIconQtip" style="margin-right:5px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" > 
                    
                    <a href="#" id="tatSetup" style="font-size:12px;" >{$page['Text']['setup']|escape:'html'}</a></td>
                 <td style="width:155px;"><input type="checkbox" {if $hideNonSbJobs!=""}checked=checked{/if} id="hideNonSbJobs" name="hideNonSbJobs">ServiceBase Jobs Only</td>
                 <td style="width:225px;">
                    {$page['Labels']['tat_view']|escape:'html'}: 
                    <input type="radio" name="ojvBy" class="ojvByElement" value="s"  {if $ojvBy neq 'd'} checked="checked" {/if} > {$page['Labels']['summary']|escape:'html'}
                    <input type="radio" name="ojvBy" class="ojvByElement"  value="d" {if $ojvBy eq 'd'}  checked="checked" {/if} > {$page['Labels']['detail']|escape:'html'}
                 </td>
             </tr>  
       </table>
        
	<table id="openJobsResults" border="0" cellpadding="0" cellspacing="0" class="browse">
	    <thead>
		<tr>
		  {foreach from=$data_keys key=kk item=vv}
                                  
                      <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="text-align:center;width:10px">
                                    <input title="Tag all visible jobs" type='checkbox' value=0 id='check_all' />
                                </th>
		</tr>
	    </thead>
               
	    <tbody>
                 
	    </tbody>
          
                <tfoot>
		<tr>
			{foreach from=$data_keys key=kk item=vv}
                                  
                      <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th><div style="cursor:pointer;" id="clearAllFilters"><img style="margin-top:3px;margin-left:2px;" src="{$_subdomain}/images/cross-icon.png"></div></th>
                               
		</tr>
	</tfoot>
           
	</table> 
                                
                                <div style="float:right;display: none" id="recordsTaggedDiv"><span id="recordsTaggedSpan"></span> Records Tagged</div>
    </div>
                        
    <div style="display:none;">

         <div id="DivStatusPreferences" class="SystemAdminFormPanel">
    
	    <form id="StatusPreferencesForm" name="StatusPreferencesForm" method="post" action="#" class="inline">

                <fieldset>
		    
                    <legend title="">{$page['Text']['status_preferences']|escape:'html'}</legend>
		    
                    <p><label id="suggestText"></label></p>
		    
		    <p style="text-align:right;">
			<a id="StatusTagAll" href="#" style="text-decoration:underline">
			    {$page['Text']['tag_all']|escape:'html'}
			</a>
			&nbsp;&nbsp;
			<a id="StatusClearAll" href="#" style="text-decoration:underline">
			    {$page['Text']['clear_all']|escape:'html'}
			</a>
			&nbsp;&nbsp;
			<input type="checkbox" id="StatusDisplaySelected" name="StatusDisplaySelected" value="1" />
			&nbsp;&nbsp;
			{$page['Text']['display_selected']|escape:'html'}
			&nbsp;&nbsp;
			<br/><br/>
		    </p>

		    <div style="height:500px;overflow-y:scroll; padding:0px; margin:0px;">
			<p>     
			    {foreach $jobStatusList as $js}
				<span>
				    <input type="checkbox" style="width:30px;" class="text StatusCheckBox" name="StatusID[]" value="{$js.StatusID|escape:'html'}" {if $js.Exists eq true}checked="checked"{/if}>
				    &nbsp;&nbsp;
				    {$js.StatusName|escape:'html'}
				<br>
				</span>
			    {/foreach} 
			</p>
		    </div>    

		    <p>
			<br/><br/>
			<span class="bottomButtons">
			    <input type="submit" name="insert_save_btn" class="btnStandard" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			    &nbsp;
			    <input type="submit" name="cancel_btn" class="btnCancel" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['cancel']|escape:'html'}" />
			    <span id="processDisplayText" style="color:red;display:none;">
				<img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" />
				&nbsp;&nbsp;
				{$page['Buttons']['process_record']|escape:'html'}
			    </span>    
			</span>
		    </p>

		</fieldset>    

	    </form>        

	</div>
                                    
                                    
	<div id="DivPreferential" class="SystemAdminFormPanel">
    
	    <form id="PreferentialForm" name="PreferentialForm" method="post" action="#" class="inline">
                
                <fieldset>
		    
                    <legend title="">{$page['Text']['preferred_client_group']|escape:'html'}</legend>                     
		    <p>
                        {* Added tabs for Network,Client,Branch,Unit Type and Skillset to show under setup.*}
                        <div id="openjobstabs">
                        <ul>
                            <li><a href="#{$page['Text']['networks']|escape:'html'|replace:' ':'_'}">{$page['Text']['networks']|escape:'html'|replace:' ':'_'}</a></li>
                            <li><a href="#{$page['Text']['clients']|escape:'html'|replace:' ':'_'}">{$page['Text']['clients']|escape:'html'|replace:' ':'_'}</a></li>
                            {*<li><a href="#{$page['Text']['brands']|escape:'html'|replace:' ':'_'}">{$page['Text']['brands']|escape:'html'}</a></li>
                            <li><a href="#{$page['Text']['branches']|escape:'html'|replace:' ':'_'}">{$page['Text']['branches']|escape:'html'|replace:' ':'_'}</a></li>*}
                            <li><a href="#{$page['Text']['manufacturers']|escape:'html'|replace:' ':'_'}">{$page['Text']['manufacturers']|escape:'html'}</a></li>
                            <li><a href="#{$page['Text']['service_providers']|escape:'html'|replace:' ':'_'}">{$page['Text']['service_providers']|escape:'html'}</a></li>
                            <li><a href="#{$page['Text']['skill_set']|escape:'html'|replace:' ':'_'}">{$page['Text']['skill_set']|escape:'html'}</a></li>
                            <li><a href="#{$page['Text']['unit_types']|escape:'html'|replace:' ':'_'}">{$page['Text']['unit_types']|escape:'html'}</a></li>
                        </ul>
                        <div class="clear"></div>
                        <div id="{$page['Text']['networks']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                
                                <span style="float:right;">
                                    <input type="checkbox" id="NetworkGroupEnable" name="NetworkGroupEnable" value="1" checked>Enable Group
                                    &nbsp;&nbsp;
                                    <a id="NetworkTagAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['tag_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <a id="NetworkClearAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['clear_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <input type="checkbox" id="NetworkDisplaySelected" name="NetworkDisplaySelected" value="1">
                                    &nbsp;&nbsp;
                                    {$page['Text']['display_selected']|escape:'html'}
                                    &nbsp;&nbsp;
                                </span>
                                <br/><br/>
                                {foreach $networkList as $nl}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text NetworkCheckBox" name="NetworkID[]" value="{$nl.NetworkID|escape:'html'}" {if $nl.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$nl.CompanyName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $nl.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="NetworkPriority{$nl.NetworkID|escape:'html'}" name="NetworkPriority{$nl.NetworkID|escape:'html'}" value="{$nl.Priority|escape:'html'}" />
                                        <br/>
                                    </span>
                                {/foreach}
                            </div>
                        </div>
                            <div id="{$page['Text']['clients']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                <span style="float:right;">
                                    <input type="checkbox" id="ClientGroupEnable" name="ClientGroupEnable" value="1" checked>Enable Group
                                    &nbsp;&nbsp;
                                    <a id="ClientTagAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['tag_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <a id="ClientClearAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['clear_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <input type="checkbox" id="ClientDisplaySelected" name="BranchDisplaySelected" value="1">
                                    &nbsp;&nbsp;
                                    {$page['Text']['display_selected']|escape:'html'}
                                    &nbsp;&nbsp;
                                </span>
                                <br/><br/>
                                {foreach $clientList as $clnt}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text ClientCheckBox" name="ClientID[]" value="{$clnt.ID|escape:'html'}" {if $clnt.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$clnt.CompanyName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $clnt.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="ClientPriority{$clnt.ID|escape:'html'}" name="ClientPriority{$clnt.ID|escape:'html'}" value="{$clnt.Priority|escape:'html'}" />
                                        <br/>
                                    </span>
                                {/foreach}
                            </div>
                        </div>
                        {*<div id="{$page['Text']['brands']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                <span style="float:right;">
                                    <input type="checkbox" id="BrandGroupEnable" name="BrandGroupEnable" value="1" checked>Enable Group
                                    &nbsp;&nbsp;
                                    <a id="BrandTagAll" href="#" style="text-decoration:underline">
					    {$page['Text']['tag_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <a id="BrandClearAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['clear_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <input type="checkbox" id="BrandDisplaySelected" name="BrandDisplaySelected" value="1" />
                                    &nbsp;&nbsp;
                                    {$page['Text']['display_selected']|escape:'html'}
                                    &nbsp;&nbsp;
                                </span>  
                                <br/><br/>
                                {foreach $brandsList as $bl}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text BrandCheckBox" name="BrandID[]" value="{$bl.BrandID|escape:'html'}" {if $bl.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$bl.BrandName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $bl.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="BrandPriority{$bl.BrandID|escape:'html'}" name="BrandPriority{$bl.BrandID|escape:'html'}" value="{$bl.Priority|escape:'html'}" />
                                        <br>
                                    </span>
                                {/foreach}
                            </div>
                        </div>
                        <div id="{$page['Text']['branches']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                <span style="float:right;">
                                    <input type="checkbox" id="BranchGroupEnable" name="BranchGroupEnable" value="1" checked>Enable Group&nbsp;&nbsp;
                                    <a id="BranchTagAll" href="#" style="text-decoration:underline">{$page['Text']['tag_all']|escape:'html'}</a>&nbsp;&nbsp;
                                    <a id="BranchClearAll" href="#" style="text-decoration:underline">{$page['Text']['clear_all']|escape:'html'}</a>&nbsp;&nbsp;
                                    <input type="checkbox" id="BranchDisplaySelected" name="BranchDisplaySelected" value="1">&nbsp;&nbsp;{$page['Text']['display_selected']|escape:'html'}&nbsp;&nbsp;
                                </span>
                                <br/><br/>
                                {foreach $branchList as $branch}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text BranchCheckBox" name="BranchID[]" value="{$branch.BranchID|escape:'html'}" {if $branch.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$branch.BranchName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $branch.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="BranchPriority{$branch.BranchID|escape:'html'}" name="BranchPriority{$branch.BranchID|escape:'html'}" value="{$branch.Priority|escape:'html'}" />
                                        <br/>
                                    </span>
                                {/foreach}
                            </div>
                        </div>*}
                        <div id="{$page['Text']['manufacturers']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                <span style="float:right;">
                                    <input type="checkbox" id="ManufacturerGroupEnable" name="ManufacturerGroupEnable" value="1" checked>Enable Group
                                    &nbsp;&nbsp;
                                    <a id="ManufacturerTagAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['tag_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <a id="ManufacturerClearAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['clear_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <input type="checkbox" id="ManufacturerDisplaySelected" name="ManufacturerDisplaySelected" value="1">
                                    &nbsp;&nbsp;
                                    {$page['Text']['display_selected']|escape:'html'}
                                    &nbsp;&nbsp;
                                </span>
                                <br/><br/>
                                {foreach $manufacturerList as $ml}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text ManufacturerCheckBox" name="ManufacturerID[]" value="{$ml.ManufacturerID|escape:'html'}" {if $ml.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$ml.ManufacturerName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $ml.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="ManufacturerPriority{$ml.ManufacturerID|escape:'html'}" name="ManufacturerPriority{$ml.ManufacturerID|escape:'html'}" value="{$ml.Priority|escape:'html'}" />
                                        <br/>
                                    </span>
                                {/foreach}
                            </div>
                        </div>
                        <div id="{$page['Text']['service_providers']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                <span style="float:right;">
                                    <input type="checkbox" id="ServiceProviderGroupEnable" name="ServiceProviderGroupEnable" value="1" checked>Enable Group
                                    &nbsp;&nbsp;
                                    <a id="ServiceProviderTagAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['tag_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <a id="ServiceProviderClearAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['clear_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <input type="checkbox" id="ServiceProviderDisplaySelected" name="ServiceProviderDisplaySelected" value="1" />
                                    &nbsp;&nbsp;
                                    {$page['Text']['display_selected']|escape:'html'}
                                    &nbsp;&nbsp;
                                </span>
                                <br/><br/>
                                {foreach $serviceProvidersList as $spl}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text ServiceProviderCheckBox" name="ServiceProviderID[]" value="{$spl.ServiceProviderID|escape:'html'}" {if $spl.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$spl.CompanyName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $spl.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="ServiceProviderPriority{$spl.ServiceProviderID|escape:'html'}" name="ServiceProviderPriority{$spl.ServiceProviderID|escape:'html'}" value="{$spl.Priority|escape:'html'}" />
                                        <br/>
                                    </span>
                                {/foreach}
                            </div>
                        </div>
                        <div id="{$page['Text']['skill_set']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                <span style="float:right;">
                                    <input type="checkbox" id="SkillSetGroupEnable" name="SkillSetEnable" value="1" checked>Enable Group
                                    &nbsp;&nbsp;
                                    <a id="SkillSetTagAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['tag_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <a id="SkillSetClearAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['clear_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <input type="checkbox" id="SkillSetDisplaySelected" name="SkillSetDisplaySelected" value="1">
                                    &nbsp;&nbsp;
                                    {$page['Text']['display_selected']|escape:'html'}
                                    &nbsp;&nbsp;
                                </span>
                                <br/><br/>
                                {foreach $skillSetList as $skill}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text SkillSetCheckBox" name="SkillSetID[]" value="{$skill.RepairSkillID|escape:'html'}" {if $skill.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$skill.RepairSkillName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $skill.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="SkillSetPriority{$skill.RepairSkillID|escape:'html'}" name="SkillSetPriority{$skill.RepairSkillID|escape:'html'}" value="{$skill.Priority|escape:'html'}" />
                                        <br/>
                                    </span>
                                {/foreach}
                            </div>
                        </div>
                        <div id="{$page['Text']['unit_types']|escape:'html'|replace:' ':'_'}">
                            <div style="height:500px;overflow-y: scroll;padding:0px;margin:0px;">
                                <span style="float:right;">
                                    <input type="checkbox" id="UnitTypeGroupEnable" name="UnitTypeGroupEnable" value="1" checked>Enable Group
                                    &nbsp;&nbsp;
                                    <a id="UnitTypeTagAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['tag_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <a id="UnitTypeClearAll" href="#" style="text-decoration:underline">
                                        {$page['Text']['clear_all']|escape:'html'}
                                    </a>
                                    &nbsp;&nbsp;
                                    <input type="checkbox" id="UnitTypeDisplaySelected" name="UnitTypeDisplaySelected" value="1">
                                    &nbsp;&nbsp;
                                    {$page['Text']['display_selected']|escape:'html'}
                                    &nbsp;&nbsp;
                                </span>
                                <br/><br/>
                                {foreach $unitTypesList as $unt}
                                    <span>
                                        <input type="checkbox" style="width:30px;" class="text UnitTypeCheckBox" name="UnitTypeID[]" value="{$unt.UnitTypeID|escape:'html'}" {if $unt.Exists eq true}checked="checked"{/if} />
                                        &nbsp;&nbsp;
                                        {$unt.UnitTypeName|escape:'html'}
                                        &nbsp;&nbsp;
                                        <input type="text" {if $unt.Exists eq true} style="width:20px;padding:0px;" {else} style="width:20px;display:none;padding:0px;" {/if} class="text" id="UnitTypePriority{$unt.UnitTypeID|escape:'html'}" name="UnitTypePriority{$unt.UnitTypeID|escape:'html'}" value="{$unt.Priority|escape:'html'}" />
                                        <br/>
                                    </span>
                                {/foreach}
                            </div>
                        </div>
                        </div>
		    </p>
		    
		    <p>&nbsp;</p>
		    
		    <p style="text-align:center;">
			<input type="submit" name="insert_save_btn2" class="btnStandard" id="insert_save_btn2" value="{$page['Buttons']['save']|escape:'html'}" />
			&nbsp;
			<input type="submit" name="cancel_btn2" class="btnCancel" id="cancel_btn2" onclick="return false;" value="{$page['Buttons']['cancel']|escape:'html'}" />
			<span id="processDisplayText2" style="color:red;display:none;">
			    <img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" />
			    &nbsp;&nbsp;
			    {$page['Buttons']['process_record']|escape:'html'}
			</span>    
		    </p>

                </fieldset>    

	    </form>        

        </div> 
                        
                        
         
                        
        <div id="DivTatSetup" class="SystemAdminFormPanel" >
    
	    <form id="TatSetupForm" name="TatSetupForm" method="post" action="#" class="inline" >

                <fieldset>
		    
                    <legend title="" >{$page['Text']['open_tat_defaults']|escape:'html'}</legend>
		    
                    <p>&nbsp;</p>
                    
                    <p style="font-weight:bold;" >
                        <label class="fieldLabel" >&nbsp;</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;{$page['Text']['open_date_range']|escape:'html'}
                        <span style="float:right;margin-right:100px;" > 
                           {$page['Text']['color']|escape:'html'} 
                        </span>    
                    </p>
                    
                    
                    <p>     
                        <label class="fieldLabel" >{$page['Labels']['button']|escape:'html'} 1:</label>
                           &nbsp;&nbsp;&nbsp;&nbsp;     
                        {$page['Text']['less_than']|escape:'html'} <input type="text" style="width:30px;" name="Button1" id="Button1"  class="text" value="{$TatResult['Button1']|escape:'html'}" > {$page['Text']['days_text']|escape:'html'}        
                       
                       <span style="float:right;margin-right:100px;" > 
                       <input  type="hidden"  style="width:0px;"  name="Button1Colour" value="{$TatResult['Button1Colour']|escape:'html'}" id="Button1Colour"  > 
                       </span>
                    </p>
		    
                    <p id="Button1Error" ></p>
                    
                     <p>     
                        <label class="fieldLabel" >{$page['Labels']['button']|escape:'html'} 2:</label>
                           &nbsp;&nbsp;&nbsp;&nbsp;     
                        {$page['Text']['between']|escape:'html'} <input type="text" style="width:30px;" name="Button1_1" id="Button1_1" readonly="readonly" class="text" value="{$TatResult['Button1']|escape:'html'}" > &  <input type="text" style="width:30px;" name="Button3_1" id="Button3_1" class="text" value="{$TatResult['Button3']|escape:'html'}" readonly="readonly" > {$page['Text']['days_text']|escape:'html'}      
                       
                       <span style="float:right;margin-right:100px;" > 
                       <input  type="hidden"  style="width:0px;"  name="Button2Colour" value="{$TatResult['Button2Colour']|escape:'html'}" id="Button2Colour"  > 
                       </span>
                    </p>
                    
                    <p id="Button2Error" ></p>
                    
                    <p>     
                        <label class="fieldLabel" >{$page['Labels']['button']|escape:'html'} 3:</label>
                           &nbsp;&nbsp;&nbsp;&nbsp;     
                            {$page['Text']['more_than']|escape:'html'} <input type="text" style="width:30px;" name="Button3" id="Button3" class="text" value="{$TatResult['Button3']|escape:'html'}" >  
                       
                       <span style="float:right;margin-right:100px;" > 
                       <input  type="hidden"  style="width:0px;"  name="Button3Colour" value="{$TatResult['Button3Colour']|escape:'html'}" id="Button3Colour"  > 
                       </span>
                    </p>
                    
                    <p id="Button3Error" ></p>
                    
                    <p>     
                        <label class="fieldLabel" >{$page['Labels']['button']|escape:'html'} 4:</label>
                           &nbsp;&nbsp;&nbsp;&nbsp;     
                            {$page['Text']['more_than']|escape:'html'} <input type="text" style="width:30px;" name="Button4" id="Button4" class="text" value="{$TatResult['Button4']|escape:'html'}" >  
                       
                       <span style="float:right;margin-right:100px;" > 
                       <input  type="hidden"  style="width:0px;"  name="Button4Colour" value="{$TatResult['Button4Colour']|escape:'html'}" id="Button4Colour"  > 
                       </span>
                    </p>
                    
                    <p id="Button4Error" ></p>
                    
		    <p>
			<br/><br/>
			<span class="bottomButtons">
			    <input type="hidden" name="TatID" value="{$TatResult['TatID']|escape:'html'}" />
                            <input type="hidden" name="TatType" value="{$TatResult['TatType']|escape:'html'}" />
                            
                            <input type="submit" name="insert_save_btn3" class="btnStandard" id="insert_save_btn3" value="{$page['Buttons']['save']|escape:'html'}" />
			    &nbsp;
			    <input type="submit" name="cancel_btn3" class="btnCancel" id="cancel_btn3" onclick="return false;" value="{$page['Buttons']['cancel']|escape:'html'}" />
			    <span id="processDisplayText3" style="color:red;display:none;">
				<img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" />
				&nbsp;&nbsp;
				{$page['Buttons']['process_record']|escape:'html'}
			    </span>    
			</span>
		    </p>

		</fieldset>    

	    </form>        

	</div>                 
                        
                        

    </div>
                                    
</div>
			    
			    
{/block}
