function validateProductForm() {
    if ($('#productSearch').val()=='') {        
        $('#productError').html('Please enter a search term').show('slow');
        $('#productSearch').focus();
        return false;
    }
    return true;
}

function setLabel( id, text ) {
    var label = $('label[for=' + id + ']');
    $(label).html(text).css('margin-right','-'+label.width()+'px');  
}

$(document).ready(function(){

    $('input[type=text]').jLabel().attr('autocomplete','off').blur(function() {$('.formError').hide('slow');} );
    $('form:first *:input[type!=hidden]:first').focus();            

    $('#productSearchForm input[name=productGroup]').click(function(){
        
       //log.info( 'clicked -> ' + this.value ); 
       switch(this.value){
           case 'ProductNo':
             setLabel('productSearch','Please enter a stock code or select a different option');
             break;
           case 'ModelName':
               setLabel('productSearch','Please enter a model number or select a different option');
               break;
           case 'ModelDescription':
               setLabel('productSearch','Please enter a item description or select a different option');
               break;    
           default:
               setLabel('productSearch','jLabel Error');
       }
    });
     
  

    $('#ProductNo, #Description, #ModelNo').keypress(function(e) {
        if(e.which == 13) {
            $(this).blur();
            if (validateProductForm()) $('#productAdvancedSearchForm').submit();
            return false;
        }
    } );    
    
    $('#Description').keydown(function(){
        $('.formError').hide('slow');
    });
    $('#Manufacturer, #Type').change(function(){
        $('.formError').hide('slow');
    });
    
});
