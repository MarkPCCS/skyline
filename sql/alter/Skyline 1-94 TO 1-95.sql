# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-12 15:45                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.94');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `customer` DROP FOREIGN KEY `county_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `country_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `customer_title_TO_customer`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `customer_TO_town_allocation`;

# ---------------------------------------------------------------------- #
# Modify table "customer"                                                #
# ---------------------------------------------------------------------- #

CREATE INDEX `IDX_customer_4` ON `customer` (`PostalCode`);

CREATE INDEX `IDX_customer_5` ON `customer` (`ContactLastName`);

# ---------------------------------------------------------------------- #
# Modify table "non_skyline_job"                                         #
# ---------------------------------------------------------------------- #

ALTER TABLE `non_skyline_job` DROP COLUMN `ServiceTypeID`;

ALTER TABLE `non_skyline_job` ADD COLUMN `ServiceType` VARCHAR(50);

ALTER TABLE `non_skyline_job` MODIFY `JobType` VARCHAR(50);

ALTER TABLE `non_skyline_job` MODIFY `ServiceType` VARCHAR(50) AFTER `JobTypeID`;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `customer` ADD CONSTRAINT `county_TO_customer` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `customer` ADD CONSTRAINT `country_TO_customer` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `customer` ADD CONSTRAINT `customer_title_TO_customer` 
    FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `customer_TO_town_allocation` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.95');
