<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Pricing Structure Page in Product Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class PricingStructure extends CustomModel {
    
    private $conn;
    
    private $table                      = "unit_pricing_structure";
    private $table_network              = "network";
    private $table_client               = "client";
    
    private $tables                     = "unit_pricing_structure AS T1 LEFT JOIN client AS T2 ON T1.ClientID=T2.ClientID LEFT JOIN unit_type AS T3 ON T1.UnitTypeID=T3.UnitTypeID LEFT JOIN manufacturer AS T4 ON T1.ManufacturerID=T4.ManufacturerID LEFT JOIN completion_status AS T5 ON T1.CompletionStatusID=T5.CompletionStatusID";
    private $dbTableColumns             = array('T1.UnitPricingStructureID', 'T2.ClientName', 'T3.UnitTypeName', 'T4.ManufacturerName', 'T1.JobSite', 'T5.CompletionStatus', 'T1.ServiceRate', 'T1.ReferralFee', 'T1.Status');
    
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables 
     * 
     * @global $this->dbTableColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        
        $NetworkID     = isset($args['firstArg'])?$args['firstArg']:'';
        $ClientID      = isset($args['secondArg'])?$args['secondArg']:'';
        
        
        if($NetworkID!='')
        {
                $args['where']    = "T1.NetworkID='".$NetworkID."'";
            
        }
        
        if($ClientID!='')
        {
            if(isset($args['where']) && $args['where'])
            {
                $args['where'] .= " AND T1.ClientID='".$ClientID."'";
            }
            else 
            {
                $args['where']    = "T1.ClientID='".$ClientID."'";
            }
            
        }
        
        if($NetworkID=='' && $ClientID=='')
        {
            $args['where']    = "T1.UnitPricingStructureID='0'";
        }    
        
        $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbTableColumns, $args);
        
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['UnitPricingStructureID']) || !$args['UnitPricingStructureID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
    
   
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
         
                $result = false;
                    
                /* Execute a prepared statement by passing an array of values */
                $sql = 'INSERT INTO '.$this->table.' (NetworkID, ClientID, UnitTypeID, ManufacturerID, JobSite, CompletionStatusID, ServiceRate, ReferralFee, Status, CreatedDate, ModifiedUserID, ModifiedDate)
                VALUES(:NetworkID, :ClientID, :UnitTypeID, :ManufacturerID, :JobSite, :CompletionStatusID, :ServiceRate, :ReferralFee, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';

                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                $result =  $insertQuery->execute(array(

                    ':NetworkID' => $args['NetworkID'],
                    ':ClientID' => $args['ClientID'], 
                    ':UnitTypeID' => $args['UnitTypeID'], 
                    ':ManufacturerID' => $args['ManufacturerID'], 
                    ':JobSite' => $args['JobSite'], 
                    ':CompletionStatusID' => $args['CompletionStatusID'], 
                    ':ServiceRate' => $args['ServiceRate'], 
                    ':ReferralFee' => $args['ReferralFee'],                     
                    ':Status' => $args['Status'],
                    ':CreatedDate' => date("Y-m-d H:i:s"),
                    ':ModifiedUserID' => $this->controller->user->UserID,
                    ':ModifiedDate' => date("Y-m-d H:i:s")

                    ));
                

                if($result)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_inserted_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
       
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * @global $this->table  
     * @global $this->table_network  
     * @global $this->table_client  
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT NetworkID, ClientID, UnitTypeID, ManufacturerID, JobSite, CompletionStatusID, ServiceRate, ReferralFee, Status, UnitPricingStructureID FROM '.$this->table.' WHERE UnitPricingStructureID=:UnitPricingStructureID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':UnitPricingStructureID' => $args['UnitPricingStructureID']));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && isset($result['NetworkID']))
        {
            //Getting network name.
            $sql3        = "SELECT CompanyName FROM ".$this->table_network." WHERE NetworkID=:NetworkID AND Status='".$this->controller->statuses[0]['Code']."'";
            $fetchQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery3->execute(array(':NetworkID' => $result['NetworkID']));
            $result3     = $fetchQuery3->fetch();
            $result['NetworkName']  = isset($result3['CompanyName'])?$result3['CompanyName']:'';
            
        }
        
        if(is_array($result) && isset($result['ClientID']))
        {
            //Getting client name.
            $sql4        = "SELECT ClientName FROM ".$this->table_client." WHERE ClientID=:ClientID AND Status='".$this->controller->statuses[0]['Code']."'";
            $fetchQuery4 = $this->conn->prepare($sql4, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery4->execute(array(':ClientID' => $result['ClientID']));
            $result4     = $fetchQuery4->fetch();
            $result['ClientName']  = isset($result4['ClientName'])?$result4['ClientName']:'';
            
        }
     
        
        return $result;
     }
    
     
    
     
     
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
        
     * @global $this->table 
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
               
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($this->controller->statuses[1]['Code']==$args['Status'])
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
            }




            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET NetworkID=:NetworkID, ClientID=:ClientID, UnitTypeID=:UnitTypeID, ManufacturerID=:ManufacturerID, JobSite=:JobSite, CompletionStatusID=:CompletionStatusID, ServiceRate=:ServiceRate, ReferralFee=:ReferralFee, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE UnitPricingStructureID=:UnitPricingStructureID';


            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $updateQuery->execute(


                    array(

                            ':NetworkID' => $args['NetworkID'],
                            ':ClientID' => $args['ClientID'], 
                            ':UnitTypeID' => $args['UnitTypeID'], 
                            ':ManufacturerID' => $args['ManufacturerID'], 
                            ':JobSite' => $args['JobSite'], 
                            ':CompletionStatusID' => $args['CompletionStatusID'], 
                            ':ServiceRate' => $args['ServiceRate'], 
                            ':ReferralFee' => $args['ReferralFee'],  
                            ':Status' => $args['Status'],
                            ':EndDate' => $EndDate,
                            ':ModifiedUserID' => $this->controller->user->UserID,
                            ':ModifiedDate' => date("Y-m-d H:i:s"),
                            ':UnitPricingStructureID' => $args['UnitPricingStructureID']  

                        )

                    );






            if($result)
            {
                    return array('status' => 'OK',
                            'message' => $this->controller->page['Text']['data_updated_msg']);
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => $this->controller->page['Errors']['data_not_processed']);
            }
              
       
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
   
    
    
}
?>